class AdminMailer < ApplicationMailer
  helper :application

  def reimbursement_notice(requester, reimbursement)
    @requester = requester
    @reimbursement = reimbursement
    @event = Event.friendly.find(reimbursement['event_slug'])

    mail(
      to: 'hi@handl.in',
      subject: "Permohonan Pencairan Dana #{@event.name}"
    )
  end
end
