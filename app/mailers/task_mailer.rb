class TaskMailer < ApplicationMailer
  helper :application

  def reminder_for(user_id)
    user = User.find(user_id)
    @unfinished_tasks_per_event = user.tasks.unfinished.includes(:event).order(due_date: :asc).group_by { |t| t.event.name }

    mail to: user.email, subject: 'Here is what on your plate'
  end
end
