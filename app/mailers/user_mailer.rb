class UserMailer < Devise::Mailer
  helper :application
  include Devise::Controllers::UrlHelpers
  default template_path: 'user_mailer'
  default from: 'Handl <support@mailer.handl.in>',
          reply_to: 'Handl <hi@handl.in>'
  layout 'mailer'

  def confirmation_instructions(record, token, options = {})
    super
  end

  def reimbursement_notice(requester, committees, reimbursement)
    @requester = requester
    @reimbursement = reimbursement
    @event = Event.friendly.find(reimbursement['event_slug'])

    mail(
      to: committees,
      subject: "Permohonan Pencairan Dana #{@event.name}",
    )
  end
end
