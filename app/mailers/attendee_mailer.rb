class AttendeeMailer < ApplicationMailer
  helper ApplicationHelper

  def registration_success(attendee_id)
    attendee_email = Attendee.where(id: attendee_id).pluck(:email)
    @attendee = Attendee.find(attendee_id)
    @event = @attendee.event
    @order = @attendee.order
    @payment_payload = DokuPayment.new(@order)
    set_event_logo

    mail to: attendee_email, subject: "Kamu telah terdaftar di acara #{@event.name}"
  end

  def confirm_attendance(attendee_id)
    attendee_email, @attendee_token = Attendee.where(id: attendee_id).pluck(:email, :token).flatten
    attendee_email = Attendee.where(id: attendee_id).pluck(:email)
    @attendee = Attendee.find(attendee_id)
    @event = @attendee.event
    set_event_logo

    mail to: attendee_email, subject: "Segera konfirmasi kehadiranmu di acara #{@event.name}"
  end

  def send_ticket(attendee_id)
    attendee = Attendee.find(attendee_id)
    attendee_email, @attendee_token = Attendee.where(id: attendee_id).pluck(:email, :token).flatten
    attendee_email = Attendee.where(id: attendee_id).pluck(:email)
    @attendee = Attendee.find(attendee_id)
    @event = @attendee.event
    set_event_logo

    pdf_ticket = WickedPdf.new.pdf_from_string(
      render_to_string(
        "tickets/ticket_template.pdf.erb",
        locals: { attendee: attendee, event: attendee.event },
      ),
      footer:  {
        content: render_to_string('tickets/_footer.pdf.erb'),
        spacing: -7 },
    )

    attachments["#{attendee.name.parameterize}.pdf"] = {
      :mime_type => 'application/pdf',
      :content => pdf_ticket
    }

    mail to: attendee.email, subject: "Tiket #{@event.name}"
  end

  def attendee_blast(email_blast, attendee)
      @attendee = attendee
      @event = attendee.event
      set_event_logo
      @content = email_blast.content
      @content = @content.gsub("<<NAME>>","#{@attendee.name}")
      @content = @content.gsub("<<FIRST_NAME>>","#{@attendee.name.sub(" ","%").split("%")[0]}")
      @content = @content.gsub("<<LAST_NAME>>","#{@attendee.name.sub(" ","%").split("%")[1]}")
      @subject = email_blast.subject
      @subject = @subject.gsub("<<NAME>>","#{@attendee.name}")
      @subject = @subject.gsub("<<FIRST_NAME>>","#{@attendee.name.sub(" ","%").split("%")[0]}")
      @subject = @subject.gsub("<<LAST_NAME>>","#{@attendee.name.sub(" ","%").split("%")[1]}")
      mail from: @event.name + '<support@mailer.handl.in>',to: attendee.email, subject: @subject
  end

  def reminder(attendee_id)
    @attendee = Attendee.find_by(id: attendee_id)
    @event = @attendee.event
    set_event_logo

    mail to: @attendee.email, subject: "Reminder #{@event.name}"
  end

  def set_event_logo
    logo_file = @event.logo.file
    if logo_file.present?
      @event_logo_title = logo_file.filename
    else
      @event_logo_title = 'logogram-dark.png'
    end
  end
end
