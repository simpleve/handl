class ApplicationMailer < ActionMailer::Base
  default from: 'Handl <support@mailer.handl.in>',
          reply_to: 'Handl <hi@handl.in>'

  layout 'mailer'
end
