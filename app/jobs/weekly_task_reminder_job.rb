class WeeklyTaskReminderJob < ActiveJob::Base
  queue_as :default

  def perform(user_id)
    TaskMailer.reminder_for(user_id).deliver_later
  end
end
