class EventReminderJob < ActiveJob::Base
  queue_as :default

  def perform(attendee_id)
    AttendeeMailer.reminder(attendee_id)
  end
end
