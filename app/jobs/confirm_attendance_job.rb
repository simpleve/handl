class ConfirmAttendanceJob < ActiveJob::Base
  queue_as :default

  def perform(event_id)
    TicketCategory.where(event_id: event_id, price: 0).each do |ticket|
      ticket.attendees.unconfirmed.limit(ticket.quantity)
            .each(&:send_attendance_confirmation_email)
    end
  end
end
