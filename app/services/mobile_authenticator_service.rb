class MobileAuthenticatorService
  def initialize(auth)
    @id_token = auth.fetch(:id_token, nil)
    @email    = auth.fetch(:email, nil)
    @password = auth.fetch(:password, nil)
  end

  # return a hash with payload, status, and error
  def call
    payload = nil
    status  = 200
    error   = nil

    if @id_token.present?
      token_info_response = HTTParty.get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=#{@id_token}")

      if valid_token?(token_info_response)
        user = User.find_by(uid: token_info_response.parsed_response['sub'])
        payload = user.to_json
      else
        error = 'token is invalid'
      end

    elsif @email.present? && @password.present?
      user = User.find_by(email: @email)
      if user && user.valid_password?(@password)
        user.generate_authentication_token!
        user.save
        payload = user.to_json
      else
        error = 'email and password did not match'
        status = 422
        puts status
      end
    else
      error = 'credentials are empty or incomplete'
    end

    { payload: payload, status: status, error: error }
  end

  private

  def valid_token?(token_info_response)
    app_id = '26556302640-kd3s9ket7va8jvdbk2v7p93e0vtm8s50.apps.googleusercontent.com'

    token_info_response.parsed_response['aud'] == app_id && not_expired?(token_info_response.parsed_response['exp'].to_i)
  end

  def not_expired?(seconds)
    Time.at(seconds) >= Time.current
  end
end
