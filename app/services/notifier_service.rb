class NotifierService
  def initialize(recipient_id, notifiable, message)
    @recipient_id = recipient_id
    @notifiable = notifiable
    @message = message
  end

  def call
    Notification.create(
      recipient_id: @recipient_id,
      notifiable: @notifiable,
      message: @message
    )
  end
end
