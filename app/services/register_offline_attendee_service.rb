class RegisterOfflineAttendeeService
  def initialize(attendee)
    @attendee = attendee
    @order = Order.new
    @ticket_category = attendee.ticket_category
  end

  def call
    if @attendee.valid?
      @order.attendees << @attendee
      @order.order_items << OrderItem.new(ticket_category_id: @ticket_category.id, quantity: 1, price: @ticket_category.price, total: @ticket_category.price * 1)
      @order.calculate_total
      @order.set_billing_info(@attendee.email, @attendee.email)
      @order.set_order_type
      @order.set_order_status
      @order.save

      OrderProcessor.log_income(@order, @attendee.event) unless @order.total.zero?

      OrderProcessor.new(@order).send_ticket_to([@attendee])

      true
    else
      false
    end
  end
end
