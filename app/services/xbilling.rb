class Xbilling
  include HTTParty

  base_uri 'https://xbilling.meseji.com/api'

  def initialize(order)
    @order = order
    @params = { headers: { "Authorization" => "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiY29kZSI6InNhMjM4NzIiLCJyb2xlIjoiYXBwIiwiaWF0IjoxNDg2NjIzOTY3fQ.QWJ8DBMEWhNqsxFsGMhVs_WLFTQvzl9LCf95DSI-kJM"} }
  end

  def invoice_list
    self.class.get('/invoices', @params)
  end

  def create_invoice
    @params[:body] = { amount: @order.total, bank_account_id: '1' }
    response = self.class.post('/invoices', @params)
    response
  end
end
