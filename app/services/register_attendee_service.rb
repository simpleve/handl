class RegisterAttendeeService
  def initialize(attendee, ticket_category, registration_type = 'online')
    @attendee = attendee
    @ticket = ticket_category
    @registration_type = registration_type
    @order = Order.new
  end

  def call
    check_ticket_availability
    process_order
    log_cashflow
    send_ticket
  end

  private

  def check_ticket_availability
    raise TicketNotAvailable unless @ticket.quantity > 0
  end

  def process_order
    @order.event = @ticket.event
    @order.order_items << OrderItem.new(
      ticket_category: @ticket,
      quantity: 1,
      price: @ticket.price,
      total: @ticket.price
    )
    @order.status = 'completed'
    @order.order_type = "paid #{@registration_type}"
    @order.attendees << @attendee

    billed_user = @attendee
    @order.billing_name = billed_user.name
    @order.billing_email = billed_user.email
    @order.calculate_total

    @order.save
  end

  def log_cashflow
    OrderProcessor.log_income(@order, @order.event)
  end

  def send_ticket
    OrderProcessor.new(@order).send_ticket_to([@attendee])
  end
end
