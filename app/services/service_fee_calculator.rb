class ServiceFeeCalculator
  PERCENTAGE_AMOUNT = 5 / 100.00
  BASIC_SERVICE_FEE = 5000.00

  def self.call(price, number)
    if price > 100_000
      price * number * PERCENTAGE_AMOUNT
    elsif price > 10_000
      number * BASIC_SERVICE_FEE
    else
      0
    end
  end
end
