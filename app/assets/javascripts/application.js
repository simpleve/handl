// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery-ui
//= require jquery_ujs
//= require foundation
//= require jquery.datetimepicker.full.min.js
//= require list.min.js
//= require list.pagination.min.js
//= require_tree .
//= require turbolinks
//= require chosen-jquery

var initFoundationJS = function() {
  $(document).foundation();
};

var initDateTimePicker = function(){
  $('#datetimepicker_1').datetimepicker({
    format:'d M Y H:i',
    minDate:0
  });

  $('#datetimepicker_2').datetimepicker({
    format:'d M Y H:i',
    minDate:0,
  });

  $('#datetimepicker_new').datetimepicker({
    format:'d M Y H:i',
    minDate:0,
  });
};

$(document).on('turbolinks:load', function() {
  initFoundationJS();
  initDateTimePicker();

  $(".menu-toggle").click(function(){
    $(".body-wrapper").toggleClass("toggled");
    $(".sidebar").toggleClass("toggled");
    $(".sub-sidebar").toggleClass("toggled");
  });

  $(".right-menu-toggle").click(function(){
    $(".body-wrapper").toggleClass("toggled");
    $(".right-sidebar").toggleClass("toggled");
  });

  $(".left-menu-toggle").click(function(){
    $(".left-sidebar").toggleClass("toggled");
  });

  $(".sidebar-toggle").click(function(){
    $(".sidebar").toggleClass("toggled");
  });

  $(".overlay").click(function(){
    $(".body-wrapper").removeClass("toggled");
    $(".sidebar").removeClass("toggled");
    $(".sub-sidebar").removeClass("toggled");
    $(".right-sidebar").removeClass("toggled");
  });

  // dismiss flash message automatically
  $('.callout.notice, .callout.success').fadeTo(3000, 500).slideUp(500);

  // loading animation
  $('.landing-page-loader').css('display', 'none');
  $('.content').css('display', 'block');

  // initialize tawk
  var Tawk_API = Tawk_API || {};
  var Tawk_LoadStart = new Date();

  var s1 = document.createElement("script");
  var s0 = document.getElementsByTagName("script")[0];
  s1.async=true;
  s1.src='https://embed.tawk.to/58d9f638f97dd14875f5a714/default';
  s1.charset='UTF-8';
  s1.setAttribute('crossorigin','*');
  s0.parentNode.insertBefore(s1,s0);
});;

$(document).on('ajax:success', 'a.attendance_confirmation', function(status, data, xhr) {
  $("a[data-uid='" + data.attendee_id +"']").replaceWith(data.status);
});
