$(document).on('turbolinks:load', function() {
  if ($('[data-behavior=notifications-item]').length === 0) {
    return false;
  }

  $.ajax({
    url: '/notifications',
    dataType: 'JSON',
    success: function(data) {
      if (data.status === 'not_found') return;
      var items = data.map(function(item) {
        return "<li data-behavior='notification-item' data-notif-id='" + item.id + "'><a href='" + item.notifiable_url + "'>" + item.message + "</a></li>";
      });

      $('[data-behavior=unread-count]').text(items.length);

      if (items.length === 0) {
        items = "<li><a>Yay, no notifications!</a></li>";
      }

      $('[data-behavior=notifications-item]').html(items);
    },
  });
});

$(document).on('click', '[data-behavior=notification-item]', function() {
  $.ajax({
    url: '/notifications/' + $(this).data('notif-id') + '/mark_as_read',
    success: function() {
      $('[data-behavior=unread-count]').text(0);
    }
  });
});
