$(document).on('turbolinks:load', function() {
  var newCommitteeForm = $('#committee_email').autocomplete({
    source: $('#committee_email').data('source'),
    select: function(event, ui) {
            $('#committee_email').val(ui.item.email);
            return false;
    },
  });

  if (newCommitteeForm.data('ui-autocomplete') !== undefined) {
    newCommitteeForm.data('ui-autocomplete')._renderItem = function(ul, item) {
        return $('<li></li>')
            .data('item.autocomplete', item)
            .append('<a><img src="' + item.profile_picture + '" />' + item.fullname + '<br></a>')
            .appendTo(ul);
    };
  }
});
