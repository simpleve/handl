$(document).on('turbolinks:load', function() {
  var selected = [];
  var detail = false;



  $(document).ready(
    function() {
      $(".cb-all").on("change", function() {
        var attendee = document.getElementsByClassName("cb-attendee");
          if(this.checked) {
            var i;
            for (i = 0; i < attendee.length; i++) {
                selected.push(attendee[i].id);
                $(attendee[i]).prop("checked",true);
            }

            console.log(selected);
          } else {
            for (i = 0; i < attendee.length; i++) {
              selected = [];
                $(attendee[i]).prop("checked",false);
            }
            console.log(selected);
          }
      });

      $(".cb-attendee").on("change", function() {
          if(this.checked) {
            selected.push(this.id);
            console.log(selected);
          } else {
            var index = selected.indexOf(this.id)
            if (index > -1) {
              selected.splice(index, 1);
            }
            console.log(selected);
          }
      });

      $("#button-delete").on("click", function(event){
        if(confirm("Are You Sure?")){
          for (i = 0; i < selected.length; i++) {

              $.ajax({
                type: "DELETE",
                url: "/events/"+$(event.target).parent().data("event-id")+"/attendees/"+selected[i]
              });

          }
        }

      });

      $("#button-detail").on("click", function(){
        if (detail === false){
          $(".additional-info").show();
          $("#button-detail").text("Hide Detail");
          detail = true;
        } else {
          $(".additional-info").hide();
          $("#button-detail").text("Show Detail");
          detail = false;
        }
      });
    }
  );
});
