$(document).on('turbolinks:load', function() {

  function changeFieldType() {
    $('.form_custom_field_field_type').on('change', function(event) {
      // this == the element that fired the change event
      var selected = event.target.options.selectedIndex;
      console.log($(event.target).parent().prop('className'));

      $.ajax({
        type:'PATCH',
        url: "/events/"+$(event.target).parent().prop('className')+"/form_custom_fields/"+$(event.target).parent().prop('id'),
        data: { show_modal: false, form_custom_field: { event_id: $(event.target).parent().prop('className'), field_type: event.target.options[selected].value }} ,
      });
      if (this.value === 'multiple_choice' || this.value === 'dropdown'){
        $("#choice-"+$(event.target).parent().prop('id')).show();
        //document.getElementById("choice-"+$(event.target).parent().prop('id')).insertAdjacentHTML('beforeend',choice);
        $("#add-btn-"+$(event.target).parent().prop('id')).show();
        //$('#required').hide();
      } else {
        $("#choice-"+$(event.target).parent().prop('id')).hide();
        $("#add-btn-"+$(event.target).parent().prop('id')).hide();
        //$('#required').show();
      }
    });
  }

  changeFieldType();

});
