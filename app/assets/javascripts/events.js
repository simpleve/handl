$(document).on('turbolinks:load', function() {
  var preview = $(".upload-preview img");
  var preview_poster = $(".upload-preview-poster img");
  var ticket_count = $("#ticket-form").children().length;

  $("#new-ticket-btn").click(function(){
      // $("#save-ticket-btn").show();
      $.ajax({
        type: "GET",
        url: "/events/new_ticket",
        data: {ticket_count: ticket_count-1}
      });
      ticket_count+=1;
  });

  // $("#save-ticket-btn").click(function(){
  //   $("#save-ticket-btn").hide();
  //   $('#ticket-form').children().first().hide();
  // });

  $("#btn-upload").change(function(event){
    if(this.files[0].size > 1000000){
      alert("Image file too large!");
    } else{
      var input = $(event.currentTarget);
      var file = input[0].files[0];
      var reader = new FileReader();
      var logo = document.getElementById('img-prev');
      logo.style.display='block';
      var holder = document.getElementById('img-plcholder');
      holder.style.display='none';

      reader.onload = function(e){
          image_base64 = e.target.result;
          preview.attr("src", image_base64);
      };

      reader.readAsDataURL(file);
    }

   });

   $("#btn-upload-poster").change(function(event){
     if(this.files[0].size > 1000000){
       alert("Image file too large!");
     } else{
      var input = $(event.currentTarget);
      var file = input[0].files[0];
      var reader = new FileReader();
      var poster = document.getElementById('img-prev-poster');
      poster.style.display='block';
      var holder = document.getElementById('img-plcholder-poster');
      holder.style.display='none';

      reader.onload = function(e){
          image_base64 = e.target.result;
          preview_poster.attr("src", image_base64);
      };

      reader.readAsDataURL(file);
    }
    });

});
