$(document).on('turbolinks:load', function() {
  $('.chosen-select').chosen({
    width: '100%'
  });
  if ($('.chosen-container').size() > 1) {
    $('.chosen-container').last().remove();
  }
});
