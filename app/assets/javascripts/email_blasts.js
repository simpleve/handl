$(document).on('turbolinks:load', function() {
  var focus;

  function changeFocusSubject() {
    focus = document.getElementById("email-subject");
  }

  function changeFocusContent() {
    focus = document.getElementById("email-content");
  }

  function addText(event) {
      var targ = event.srcElement;

      var content = focus;
      content.value += "<<";
      content.value += targ.innerText.replace(" ","_").toUpperCase();
      content.value += ">>";
      content.focus();
  }
});
