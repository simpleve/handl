module ApplicationHelper
  def days_to(datetime)
    if datetime < DateTime.current
      'due date has passed'
    else
      date_diff = datetime.to_datetime - DateTime.current
      days_remaining = date_diff.ceil.to_i
      pluralize(days_remaining, 'day')
    end
  end

  # return day, date month year
  def format_date(datetime)
    datetime.strftime('%A, %e %B %Y')
  end

  def committee?
    if current_user
      current_user.is_committee?(params[:event_slug])
    else
      return false
    end
  end

  def show_avatar(user, size = nil)
    size_class = ['avatar', size].compact.join('-')
    if user.profile_picture.present?
      image_tag(user.profile_picture, class: size_class)
    elsif user.unconfirmed?
      render partial: 'shared/initials_avatar',
             locals: { name: user.email, size: size_class }
    else
      render partial: 'shared/initials_avatar',
             locals: { name: user.fullname, size: size_class }
    end
  end

  def activate_menu(params = {})
    if controller?(params[:controller]) && action?(params[:action])
      'active'
    else
      ''
    end
  end

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  def action?(*action)
    action.flatten.include?(params[:action])
  end

  def current_event_class(event_id, param_id)
    return 'active' if event_id == param_id
  end

  def format_datetime_range(start_datetime, end_datetime)
    if start_datetime.nil?
      "to be confirmed"
    elsif end_datetime.nil?
      "#{start_datetime.strftime('%A, %e %B %Y %H:%M')}"
    elsif (end_datetime.to_date - start_datetime.to_date).to_i > 0
      "#{start_datetime.strftime('%A, %e %B %Y %H:%M')} - #{end_datetime.strftime('%A, %e %B %Y %H:%M')}"
    else
      "#{start_datetime.strftime('%A, %e %B %Y %H:%M')} - #{end_datetime.strftime('%H:%M')}"
    end
  end
end
