class EventCommittee
  def initialize(event, user)
    @event = event
    @user = user
  end

  def is_committee?
    return false if @user.nil?
    Committee.where(event_id: @event.id, user_id: @user.id).present?
  end
end
