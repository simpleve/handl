class TicketIssuer
  def initialize(attendee, ticket_category)
    @ticket_category = ticket_category
    @attendee = attendee
  end

  def issue_ticket
    check_ticket_availability
    substract_ticket_quantity
    send_ticket_through_email
  end

  private

  def check_ticket_availability
    raise TicketNotAvailable unless @ticket_category.quantity > 0
  end

  def substract_ticket_quantity
    current_qty = @ticket_category.quantity

    updated_qty = current_qty - 1
    @ticket_category.update(quantity: updated_qty)
  end

  def send_ticket_through_email
    AttendeeMailer.send_ticket(@attendee.id).deliver_later
  end
end

class TicketNotAvailable < StandardError; end
