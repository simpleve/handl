# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  fullname               :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  provider               :string
#  uid                    :string
#  profile_picture        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  auth_token             :string
#  id_card                :string
#  bank                   :string
#  bank_acc_number        :string
#
# Indexes
#
#  index_users_on_auth_token            (auth_token)
#  index_users_on_confirmation_token    (confirmation_token) UNIQUE
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#

class User < ActiveRecord::Base
  mount_uploader :id_card, IdCardUploader

  # Include default devise modules. Others available are:
  # , :lockable, :timeoutable, :trackable, :validatable and :omniauthable
  devise :confirmable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :omniauthable,
         omniauth_providers: [:google_oauth2]

  has_many :assignments, dependent: :destroy
  has_many :tasks, through: :assignments

  has_many :committees, dependent: :destroy
  has_many :events, through: :committees

  has_many :notifications, foreign_key: :recipient_id

  # validates :fullname, presence: true
  validates :email, presence: true, uniqueness: true

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.password = Devise.friendly_token[0,20]
      user.fullname = auth.info.name
      user.profile_picture = auth.info.image
      user.confirmed_at = DateTime.now
    end
  end

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  def is_committee?(slug)
    Event.friendly.find(slug).committees.find_by(user_id:self.id)
  end

  def password_match?
     self.errors[:password] << "can't be blank" if password.blank?
     self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
     self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
     password == password_confirmation && !password.blank?
  end

  def set_fullname_from(params)
    update_attributes(fullname: params[:fullname])
  end

  # new function to set the password without knowing the current
  # password used in our confirmation controller.
  def attempt_set_password(params)
    p = {}
    p[:password] = params[:password]
    p[:password_confirmation] = params[:password_confirmation]
    update_attributes(p)
  end

  def has_no_password?
    self.encrypted_password.blank?
  end

  def only_if_unconfirmed
    pending_any_confirmation {yield}
  end

  def unconfirmed?
    confirmed_at.nil?
  end

  def generate_authentication_token!
    loop do
      self.auth_token = Devise.friendly_token
      break unless self.class.exists?(auth_token: auth_token)
    end
  end

  def eligible_for_reimbursement?
    bank.present? && bank_acc_number.present? && id_card.present?
  end
end
