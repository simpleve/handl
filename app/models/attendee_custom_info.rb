# == Schema Information
#
# Table name: attendee_custom_infos
#
#  id                      :integer          not null, primary key
#  field_name              :string
#  value                   :text
#  attendee_id             :integer
#  form_custom_field_id    :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  attachment_file_name    :string
#  attachment_content_type :string
#  attachment_file_size    :integer
#  attachment_updated_at   :datetime
#

class AttendeeCustomInfo < ActiveRecord::Base
  belongs_to :attendee
  belongs_to :form_custom_field

  has_attached_file :attachment
  do_not_validate_attachment_file_type :attachment
end
