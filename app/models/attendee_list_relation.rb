# == Schema Information
#
# Table name: attendee_list_relations
#
#  id               :integer          not null, primary key
#  attendee_id      :integer
#  attendee_list_id :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class AttendeeListRelation < ActiveRecord::Base
  belongs_to :attendee
  belongs_to :attendee_list
end
