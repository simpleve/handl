# == Schema Information
#
# Table name: attendee_lists
#
#  id         :integer          not null, primary key
#  list_name  :string
#  event_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AttendeeList < ActiveRecord::Base
    has_many :attendees, through: :attendee_list_relations
    has_many :attendee_list_relations, dependent: :destroy
    belongs_to :event
end
