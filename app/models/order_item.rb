# == Schema Information
#
# Table name: order_items
#
#  id                 :integer          not null, primary key
#  order_id           :integer
#  ticket_category_id :integer
#  quantity           :integer
#  price              :float
#  total              :float
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_order_items_on_order_id            (order_id)
#  index_order_items_on_ticket_category_id  (ticket_category_id)
#
# Foreign Keys
#
#  fk_rails_1412e1959a  (ticket_category_id => ticket_categories.id)
#  fk_rails_e3cb28f071  (order_id => orders.id)
#

class OrderItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :ticket_category

  before_save :calculate_total

  def generate_from_ticket_ordered(quantity)
    raise 'Ticket category not selected' unless ticket_category.present?

    self.price    = ticket_category.price
    self.quantity = quantity
    self.total    = price * quantity

    self
  end

  def calculate_total
    self.total = quantity * price
  end
end
