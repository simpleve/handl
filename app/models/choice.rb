# == Schema Information
#
# Table name: choices
#
#  id                   :integer          not null, primary key
#  choice_name          :string
#  form_custom_field_id :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Choice < ActiveRecord::Base
  belongs_to :form_custom_field
end
