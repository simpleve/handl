# == Schema Information
#
# Table name: email_blasts
#
#  id         :integer          not null, primary key
#  recipient  :string
#  subject    :string
#  content    :text
#  event_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EmailBlast < ActiveRecord::Base
  belongs_to :event

  def send_email(attendees)
    AttendeeMailer.attendee_blast(self, attendees)
  end
end
