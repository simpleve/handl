# == Schema Information
#
# Table name: assignments
#
#  id          :integer          not null, primary key
#  task_id     :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  accepted_at :datetime
#
# Indexes
#
#  index_assignments_on_task_id  (task_id)
#  index_assignments_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_aa6b76dac2  (user_id => users.id)
#  fk_rails_b7c53ee3b6  (task_id => tasks.id)
#

class Assignment < ActiveRecord::Base
  belongs_to :user
  belongs_to :task

  scope :pending, -> { where(accepted_at: nil) }
  scope :accepted, -> { where('accepted_at is not null') }

  def accepted?
    accepted_at.present?
  end
end
