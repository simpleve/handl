# == Schema Information
#
# Table name: cashflows
#
#  id          :integer          not null, primary key
#  amount      :decimal(10, 2)
#  event_id    :integer
#  order_id    :integer
#  account     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  detail      :string
#  sub_account :string
#
# Indexes
#
#  index_cashflows_on_event_id  (event_id)
#  index_cashflows_on_order_id  (order_id)
#
# Foreign Keys
#
#  fk_rails_0895e6ccf1  (order_id => orders.id)
#  fk_rails_8f84d233ce  (event_id => events.id)
#

class Cashflow < ActiveRecord::Base
  belongs_to :event
  belongs_to :order

  scope :filter_by_account, -> (account) { where(account: account) if account.present? }

  scope :sort_by_attribute, -> (attribute, dir = 'asc') {
    order("cashflows.updated_at #{dir}") if attribute.present?
  }
end
