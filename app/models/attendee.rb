# == Schema Information
#
# Table name: attendees
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  email              :string           not null
#  event_id           :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  ticket_category_id :integer          not null
#  status             :integer          default(0)
#  order_id           :integer
#  token              :string
#  phone              :string
#  company            :string
#
# Indexes
#
#  index_attendees_on_event_id  (event_id)
#
# Foreign Keys
#
#  fk_rails_4037bda9d5  (event_id => events.id)
#  fk_rails_7732aa5716  (order_id => orders.id)
#  fk_rails_e8a1210330  (ticket_category_id => ticket_categories.id)
#

class Attendee < ActiveRecord::Base
  belongs_to :event
  belongs_to :ticket_category
  belongs_to :order
  has_many :attendee_custom_infos, dependent: :destroy
  has_many :attendee_lists, through: :attendee_list_relations
  has_many :attendee_list_relations, dependent: :destroy

  accepts_nested_attributes_for :attendee_custom_infos

  validates :name, :email, :ticket_category, presence: true

  enum status: { unconfirmed: 0, confirmation_sent: 1, confirmed: 2, cancelled: 3, attended: 4 }

  before_create :generate_token

  def generate_token
    self.token = loop do
      token = SecureRandom.urlsafe_base64(6)
      break token unless Attendee.exists?(token: token)
    end
  end

  def send_registration_success_email
    AttendeeMailer.registration_success(id).deliver_later
  end

  def send_attendance_confirmation_email
    AttendeeMailer.confirm_attendance(id).deliver_later
    update(status: :confirmation_sent)
  end

  def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |attendee|
        csv << attendee.attributes.values_at(*column_names)
        attendee.custom_infos.each do |custom_info|
          if custom_field.file_upload?
            csv << custom_info.attachment_file_name
          elsif custom_field.multiple_choice?
            csv << Choice.find(custom_info.value).choice_name
          else
            csv << custom_info.value
          end
        end
      end
    end
  end
end
