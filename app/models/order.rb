# == Schema Information
#
# Table name: orders
#
#  id            :integer          not null, primary key
#  total         :integer
#  payment_code  :string
#  status        :string
#  billing_name  :string
#  billing_email :string
#  expired_at    :datetime
#  paid_total    :integer
#  order_type    :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  event_id      :integer
#
# Foreign Keys
#
#  fk_rails_64bd9e45d4  (event_id => events.id)
#

class Order < ActiveRecord::Base
  has_many :attendees, dependent: :destroy
  belongs_to :event
  has_many :order_items
  has_many :cashflows, dependent: :destroy
  has_one :invoice

  accepts_nested_attributes_for :attendees
  accepts_nested_attributes_for :order_items

  validate :order_not_zero
  before_create :delete_zero_order_items

  def order_not_zero
    ticket_ordered_quantity = order_items.map(&:quantity).sum

    if ticket_ordered_quantity.zero?
      errors.add(:order, 'cannot be empty')
    end
  end

  def delete_zero_order_items
    self.order_items = self.order_items.reject { |item| item.quantity == 0 }
  end

  def calculate_total
    self.total = 0
    order_items.each do |item|
      item.total = item.price * item.quantity
      self.total += item.price * item.quantity
    end
  end

  def basket
    event_name = event.name

    order_items.map do |item|
      "#{item.ticket_category.category_name} (#{event_name}),#{item.price},#{item.quantity},#{item.total}"
    end.join(';')
  end

  def set_billing_info(email, name)
    self.billing_email = email
    self.billing_name = name
  end

  def set_order_status
    self.status = free? ? 'completed' : 'payment_pending'
  end

  def set_order_type
    self.order_type = free? ? 'free' : 'paid'
  end

  def free?
    self.total.zero?
  end
end
