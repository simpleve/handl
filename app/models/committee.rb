# == Schema Information
#
# Table name: committees
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  event_id   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_committees_on_event_id  (event_id)
#  index_committees_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_2ea1709414  (event_id => events.id)
#  fk_rails_99084a02fc  (user_id => users.id)
#

class Committee < ActiveRecord::Base
  belongs_to :user
  belongs_to :event

  validates :user, uniqueness: {scope: [:event_id]}

  def username
    user.fullname
  end
end
