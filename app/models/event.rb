# == Schema Information
#
# Table name: events
#
#  id                     :integer          not null, primary key
#  name                   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  start_at               :datetime
#  logo                   :string
#  description            :string
#  location               :string
#  poster                 :string
#  organized_by           :string
#  end_at                 :datetime
#  slug                   :string
#  registration_open      :boolean          default(TRUE)
#  contact_name           :string
#  contact_email          :string
#  contact_phone          :string
#  name_desc              :string
#  company_desc           :string
#  email_desc             :string
#  phone_desc             :string
#  registration_closed_at :datetime
#  force_open             :boolean          default(FALSE)
#
# Indexes
#
#  index_events_on_slug  (slug) UNIQUE
#

class Event < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  mount_uploader :logo, LogoUploader
  mount_uploader :poster, PosterUploader

  has_many :committees, dependent: :destroy
  has_many :users, through: :committees
  # has_many :tasks, dependent: :destroy
  has_many :ticket_categories, dependent: :destroy
  has_many :orders, dependent: :destroy
  has_many :attendees, dependent: :destroy
  has_many :form_custom_fields, dependent: :destroy
  has_many :email_blasts
  has_many :attendee_lists, dependent: :destroy
  has_many :cashflows, dependent: :destroy

  accepts_nested_attributes_for :attendee_lists
  accepts_nested_attributes_for :ticket_categories

  validates :name, presence: true
end
