class OrderProcessor
  def initialize(order)
    @order = order
  end

  def confirm_payment_success
    @order.update(status: 'payment_success')
    @order.invoice.pay!
    notify_event_organizer
  end

  def send_ticket_to(attendees)
    attendees_list = attendees
    attendees_list.each do |attendee|
      attendee.update(status: :confirmed)
      TicketIssuer.new(attendee, attendee.ticket_category).issue_ticket
    end
  end

  def self.log_income(order, event)
    Cashflow.create(
      order: order,
      event: event,
      account: 'income',
      sub_account: 'ticket',
      amount: order.total
    )

    Cashflow.create(
      order: order,
      event: event,
      account: 'expense',
      sub_account: 'service_fee',
      # TODO: handle edge case when ticket ordered have 2 kind of service fee calculation, ex: 2 tickets, 1 priced 20_000, the other 200_000
      amount: ServiceFeeCalculator.call(order.total, order.attendees.count)
    )
  end

  def notify_event_organizer
    event = @order.event
    event.committees.each do |committee|
      NotifierService.new(
        committee.user_id,
        @order,
        "Order ##{@order.id} successfully paid"
      ).call
    end
  end
end
