# == Schema Information
#
# Table name: ticket_categories
#
#  id            :integer          not null, primary key
#  category_name :string
#  price         :integer
#  quantity      :integer
#  event_id      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_ticket_categories_on_event_id  (event_id)
#
# Foreign Keys
#
#  fk_rails_57e1f626f2  (event_id => events.id)
#

class TicketCategory < ActiveRecord::Base
  belongs_to :event
  has_many :attendees

  validates :category_name, :quantity, :price, presence: true
  validates :quantity, numericality: { only_integer: true }
  validates :price, numericality: true
  validate :quota_availability

  def quota_availability
    if quantity < 0
      errors.add(:ticket_category, 'has sold out')
      self.quantity = 0
    end
  end
end
