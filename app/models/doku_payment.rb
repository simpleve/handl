class DokuPayment
  PAYMENT_URL = 'https://apps.myshortcart.com/payment/request-payment/'
  STORE_ID    = '10972248'
  APP_URL     = 'https://handl.in'
  SHARED_KEY  = 'l6y5m5u5c3p8'

  attr_accessor :basket, :transid_merchant, :store_id, :amount, :url, :cname, :cemail, :word

  def initialize(order)
    @basket           = order.basket
    @transid_merchant = order.id
    @store_id         = STORE_ID
    @amount           = order.total.round(2)
    @url              = APP_URL
    @cname            = order.billing_name
    @cemail           = order.billing_email
    @word             = words(@amount, @transid_merchant)
  end

  def words(amount, transid_merchant)
    Digest::SHA1.hexdigest([amount, SHARED_KEY, transid_merchant].join)
  end
end
