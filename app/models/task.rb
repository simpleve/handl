# == Schema Information
#
# Table name: tasks
#
#  id           :integer          not null, primary key
#  name         :string           not null
#  due_date     :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  task_list_id :integer
#  done_at      :datetime
#  event_id     :integer
#
# Indexes
#
#  index_tasks_on_task_list_id  (task_list_id)
#
# Foreign Keys
#
#  fk_rails_31b279bb45  (event_id => events.id)
#

class Task < ActiveRecord::Base
  has_many :assignments, dependent: :destroy
  has_many :users, through: :assignments
  accepts_nested_attributes_for :assignments
  # belongs_to :task_list
  # belongs_to :event
  validates :name, :due_date, presence: true

  scope :unfinished, -> { where(done_at: nil) }
  scope :finished, -> { where('done_at is not null') }

  def done=(value)
    update_attributes(done_at: Time.zone.now) if value
  end

  def done
    done_at.present?
  end

  def done?
    done
  end
end
