# == Schema Information
#
# Table name: form_custom_fields
#
#  id          :integer          not null, primary key
#  name        :string
#  required    :boolean          default(FALSE)
#  event_id    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  field_type  :integer          default(0), not null
#  description :string
#

class FormCustomField < ActiveRecord::Base
  belongs_to :event
  has_many :attendee_custom_infos, dependent: :destroy
  has_many :choices

  accepts_nested_attributes_for :choices

  enum field_type: [:short_input_text, :paragraph, :file_upload, :multiple_choice, :dropdown]
end
