# == Schema Information
#
# Table name: invoices
#
#  id             :integer          not null, primary key
#  amount         :decimal(10, 2)   not null
#  status         :string           not null
#  payment_code   :integer
#  total_amount   :decimal(10, 2)   not null
#  event_id       :integer          not null
#  order_id       :integer          not null
#  expired_at     :datetime
#  uid            :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  payment_method :integer          not null
#
# Indexes
#
#  index_invoices_on_event_id  (event_id)
#  index_invoices_on_order_id  (order_id)
#
# Foreign Keys
#
#  fk_rails_4fa42a6dca  (order_id => orders.id)
#  fk_rails_aa64c7515d  (event_id => events.id)
#

class Invoice < ActiveRecord::Base
  include AASM
  belongs_to :event
  belongs_to :order

  aasm column: :status do
    state :pending, initial: true
    state :paid
    state :expired

    event :pay do
      transitions from: :pending, to: :paid
    end

    event :expire do
      transitions from: :pending, to: :expired
    end
  end

  enum payment_method: { doku: 1, xbilling: 2 }

  before_create :set_uid

  private

  def set_uid
    self.uid = "#{id}#{Date.current.strftime('%y%m%d')}#{order_id}"
  end
end
