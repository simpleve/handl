class TicketsController < ApplicationController
  def preview
    @attendee = Attendee.last
    @event = @attendee.event

    respond_to do |format|
      format.pdf do
        render pdf: "#{@attendee.name}",
                template: "tickets/ticket_template.pdf.erb",
                header:{
                  html: {
                    template: 'tickets/_header.pdf.erb'},
                  spacing: -7 },

                footer:  {
                  html: {
                    template: 'tickets/_footer.pdf.erb'},
                  spacing: -7 },

                encoding: "UTF-8",
                margin:  { top: 30, bottom: 10, left: 10, right: 10 },
                locals: { attendee: @attendee, event: @event }
      end
    end
  end

  def send_ticket
    @attendee = Attendee.find_by(id: params[:attendee_id])

    if TicketIssuer.new(@attendee, @attendee.ticket_category).issue_ticket
      flash[:success] = 'Ticket sent'
    else
      flash[:error] = 'Ticket not sent'
    end
    redirect_to event_attendees_path(event_slug: params[:event_slug])
  end
end
