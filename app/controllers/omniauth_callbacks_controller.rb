class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def google_oauth2
    @user = User.from_omniauth(request.env['omniauth.auth'])
    puts request.env['omniauth.auth']
    if @user.profile_picture.blank?
      @user.profile_picture = request.env['omniauth.auth'].info.image
      @user.save
    end
    sign_in @user
    redirect_to events_path
  end
end
