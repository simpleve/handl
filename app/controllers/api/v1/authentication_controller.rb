class API::V1::AuthenticationController < API::V1::BaseController
  skip_before_filter :authenticate

  def create
    response = MobileAuthenticatorService.new(params).call

    respond_to do |format|
      format.json { render json: response, status: response[:status] }
    end
  end
end
