class API::V1::EventsController < API::V1::BaseController
  before_action :set_event, only: [:show]

  def index
    @events = @authorized_user.events
    render(json: @events)
  end

  def show
    render(json: API::V1::EventSerializer.new(@event).to_json)
  end

  private

  def set_event
    @event = Event.find_by(slug: params[:slug])
  end
end
