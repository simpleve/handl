class API::V1::TicketCategoriesController < API::V1::BaseController
  before_action :set_event

  def index
    @tickets = @event.ticket_categories
    @ticketz = @tickets.map { |ticket| API::V1::TicketCategorySerializer.new(ticket) }
    render(json: @ticketz.to_json)
  end

  private

  def set_event
    @event = Event.friendly.find(params[:event_slug])
    raise ActiveRecord::RecordNotFound unless @event
  end
end
