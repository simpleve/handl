class API::V1::AttendeesController < API::V1::BaseController
  before_action :set_event

  def index
    @attendees = @event.attendees
    render(json: @attendees)
  end

  def verify_ticket
    attendee = Attendee.find_by(token: params[:token])

    if attendee.attended?
      render(json: { errors: 'Attendee already checked in' }, status: 404)
    elsif attendee && attendee.attended!
      render(json: attendee, status: 200)
    else
      render(json: { errors: 'Attendee not found' }, status: 404)
    end
  end

  private

  def set_event
    @event = Event.friendly.find(params[:event_slug])
    raise ActiveRecord::RecordNotFound unless @event
  end
end
