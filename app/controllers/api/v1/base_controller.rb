class API::V1::BaseController < ApplicationController
  protect_from_forgery with: :null_session
  before_action :authenticate

  protected

  def authenticate
    authenticate_token || render_unauthorized
  end

  def authenticate_token
    authenticate_with_http_token do |token, _options|
      @authorized_user = User.find_by(auth_token: token)
    end
  end

  def render_unauthorized
    respond_to do |format|
      format.json { render json: { message: 'Bad credentials' }, status: 401 }
    end
  end
end
