class AttendeeCustomInfosController < ApplicationController
  def create
    @form_custom_field = FormCustomField.new(form_custom_field_params)

    if @form_custom_field.save
      redirect_to :back
    else
      render :index
    end
  end

  def form_custom_field_params
    params.require(:attendee_custom_info).permit(:field_name, :value, :form_custom_field_id, :attachment)
  end
end
