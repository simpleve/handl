class AccountsController < ApplicationController
  before_action :authenticate_user!

  def edit
    @user = current_user
  end

  def update
    @user = current_user

    if @user.update(user_params)
      flash[:success] = 'Account successfully updated'
      redirect_to account_setting_path
    else
      render :edit
    end
  end

  private

  def user_params
    params.require(:user).permit(:profile_picture, :fullname, :email, :bank, :bank_acc_number, :id_card)
  end
end
