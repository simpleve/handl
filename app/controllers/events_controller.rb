class EventsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  layout 'sub_sidebar', except: [:index, :new]

  def index
    @events = current_user.events
    @event = Event.new
  end

  def show
  end

  def new
    @event = Event.new
    respond_to do |format|
      format.js
      format.html
    end
  end

  def create
    @event = Event.new(event_params)
    @committee = Committee.new(user: current_user, event: @event)

    if @event.save && @committee.save
      redirect_to events_path
    else
      render :new
    end
  end

  def edit
    @ticket_categories = @event.ticket_categories

    @event_slug = @event.slug
    @new_ticket = TicketCategory.new
  end

  def update
    @event.slug = nil
    if @event.update(event_params)
      if params[:from_form]
        respond_to do |format|
          format.js
        end
      elsif params[:set_registration_closed]
        if @event.registration_closed_at
          if DateTime.now < @event.registration_closed_at
            @event.registration_open = true
            change_status = true
          else
            change_status = false
            @event.registration_open = false
          end
          @event.force_open = false
          @event.save
        end
        render json: { status: :ok, message: 'registration_closed_update_success', status_changed: change_status}
      else
        flash[:success] = 'Event updated.'
        redirect_to edit_event_path(@event)
      end
    else
      render :edit
    end
  end

  def new_ticket
    respond_to do |format|
      format.js
    end
  end

  def save_ticket
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @event.destroy
    redirect_to events_path

    respond_to do |format|
      format.html
      format.json
    end
  end

  def open_close_registration
    event = Event.friendly.find(params[:event_slug])
    if event.registration_closed_at
      if params[:is_open] && (DateTime.now > event.registration_closed_at)
        event.force_open = true
      end
    end
    event.registration_open = params[:is_open]
    event.save
    redirect_to :back
  end

  def update_slug
    @event = Event.friendly.find(params[:event_slug])
    @event.update(slug: nil)
    @event.slug = params[:slug]

    if @event.save
      redirect_to event_form_custom_fields_path(event_slug: @event.slug)
    end
  end

  def overview
    @event = current_user.events.friendly.find(params[:event_slug])
    @cashflows = @event.cashflows.includes(:order)

    @income = @cashflows.where(account: 'income')
    @expense = @cashflows.where(account: 'expense')
    @total_income = @income.sum(:amount)
    @total_expense = @expense.sum(:amount)
    @balance = @total_income - @total_expense

    @event_presenter = EventPresenter.new(@event, current_user)
  end

  private

  def set_event
    @event = current_user.events.friendly.find(params[:slug])
    raise ActiveRecord::RecordNotFound unless @event
  end

  def event_params
    params.require(:event).permit(:name, :registration_open, :description, :location, :organized_by, :poster, :start_at, :end_at, :logo, :remove_logo, :name_desc, :email_desc, :phone_desc, :company_desc, :contact_name, :contact_email, :contact_phone, :registration_closed_at, attendee_lists_attributes: [:list_name], ticket_categories_attributes: [:category_name, :price, :quantity])
  end
end
