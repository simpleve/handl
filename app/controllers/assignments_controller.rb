class AssignmentsController < ApplicationController
  before_filter :set_assigment, only: [:show, :edit, :destroy, :accept_assignment]

  def index
    @assignments = current_user.assignments.includes(task: :task_list)
                                           .group_by { |a| a.task.task_list.event.name }
  end

  def show
  end

  def new
    @assignment = Assignment.new
  end

  def create
    @assignment = Assignment.new(assignment_params)

    if @assignment.save
      redirect_to @assignment
    else
      render :new
    end
  end

  def accept
    @assignment = Assignment.find(params[:assignment_id])
    @assignment.update(accepted_at: Time.zone.now)

    redirect_to my_tasks_path
  end

  private

  def set_assigment
    @assignment = Assignment.find(params[:id])
  end

  def assignment_params
    params.require(:assignment).permit(:user_id, :task_id)
  end
end
