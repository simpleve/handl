class PaymentsController < ApplicationController
  protect_from_forgery with: :null_session
  layout 'landing_page'

  def verify
    storeid         = params['STOREID']
    transidmerchant = params['TRANSIDMERCHANT']
    amount          = params['AMOUNT']
    words           = params['WORDS']

    @order = Order.find_by(
      id: transidmerchant,
      total: amount,
      status: 'payment_pending'
    )

    if @order.present?
      render plain: 'Continue'
    else
      render plain: 'Stop'
    end
  end

  def notify
    transidmerchant = params['TRANSIDMERCHANT']
    amount          = params['AMOUNT']
    result          = params['RESULT']

    @order = Order.find_by(
      id: transidmerchant,
      total: amount,
      status: 'payment_pending'
    )

    if result.upcase == 'SUCCESS' && @order.present?
      op = OrderProcessor.new(@order)
      op.confirm_payment_success
      op.send_ticket_to(@order.attendees)
      OrderProcessor.log_income(@order, @order.event)

      render plain: 'Continue'
    else
      render plain: 'Stop'
    end
  end

  def redirect
    transidmerchant = params['TRANSIDMERCHANT']
    @payment_method = params['PTYPE']
    @result = params['RESULT']

    @order = Order.find_by(id: transidmerchant)
    @event = @order.event
  end

  def cancel
    render plain: 'Canceled'
  end
end
