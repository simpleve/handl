class StaticPagesController < ApplicationController

  layout 'landing_page'

  def home

  end

  def privacy_policy
  end

  def features
  end

  def terms_and_conditions
  end

  def pricing
  end

  def about_us
  end

end
