class InvoicesController < ApplicationController
  def create
    @order = Order.find_by(id: params[:id])
    @order.calculate_total

    case params[:payment_method].to_i
    when 1
      Invoice.create(
        amount: @order.total,
        payment_code: 0,
        total_amount: @order.total,
        expired_at: DateTime.current + 12.hours,
        order_id: params[:id],
        event_id: @order.event_id,
        payment_method: params[:payment_method].to_i
      )
    when 2
      response = Xbilling.new(@order).create_invoice
      data = JSON.parse(response.body)['data']

      Invoice.create(
        amount: data['amount'],
        payment_code: data['payment_verification_number'],
        total_amount: data['total_amount'],
        expired_at: DateTime.parse(data['expire_at']),
        order_id: params[:id],
        event_id: @order.event_id,
        payment_method: params[:payment_method].to_i
      )
    end

    redirect_to order_check_out_event_order_path
  end
end
