class OrdersController < ApplicationController
  layout 'guest'

  def show
    @is_committee = @event.committees.include? current_user.committees.find_by event_id: @event.id
  end

  def update
  end

  def new
    @event = Event.friendly.find(params[:event_slug])
    if @event.registration_closed_at
      if (DateTime.now > @event.registration_closed_at) && @event.force_open == false
        @event.registration_open = false
        @event.save
      end
    end
    @event_presenter = EventPresenter.new(@event, current_user)
    @order = Order.new
    initiate_order_items
  end

  def create
    @order = Order.new(order_params)
    @order.status = 'incomplete_order'
    @order.calculate_total
    @order.set_order_type

    if @order.save
      redirect_to order_attendee_form_event_order_path(id: @order.id)
    else
      @event = Event.friendly.find(params[:event_slug])
      @event_presenter = EventPresenter.new(@event, current_user)
      render :new
    end
  end

  def attendee_form
    @event = Event.includes(form_custom_fields: :choices).friendly.find(params[:event_slug])
    @event_presenter = EventPresenter.new(@event)
    @order = Order.find_by(id: params[:id])
    initiate_attendees
  end

  def submit_attendee
    @event = Event.includes(form_custom_fields: :choices).friendly.find(params[:event_slug])
    @event_presenter = EventPresenter.new(@event)
    @order = Order.find_by(id: params[:id])
    @order.attendees_attributes = params[:order][:attendees_attributes]

    if @order.save
      if @order.free?
        redirect_to order_check_out_event_order_path
      else
        redirect_to payment_methods_event_order_path
      end
    else
      initiate_attendees
      render 'attendee_form'
    end
  end

  def payment_methods
    @event = Event.includes(form_custom_fields: :choices).friendly.find(params[:event_slug])
    @event_presenter = EventPresenter.new(@event)
    @order = Order.find_by(id: params[:id])
  end

  def check_out
    @event = Event.friendly.find(params[:event_slug])
    @order = Order.find_by(id: params[:id])
    @order.set_order_status
    @billing_user = @order.attendees.first
    @order.set_billing_info(@billing_user.email, @billing_user.name)
    @order.save
    @order.attendees.each(&:send_registration_success_email)
    @payment_payload = DokuPayment.new(@order)
    @invoice = @order.invoice
  end

  private

  def order_params
    params.require(:order).permit(:event_id, :total, :billing_name, :billing_email, :status, order_items_attributes: [:id, :quantity, :price, :ticket_category_id, :total], attendees_attributes: [:ticket_category_id, :name, :email, :phone, :event_id, :company, attendee_custom_infos_attributes: [:field_name, :value, :form_custom_field_id, :attachment]])
  end

  def initiate_order_items
    @event.ticket_categories.each do |ticket|
      @order.order_items.build do |item|
        item.ticket_category = ticket
        item.price           = ticket.price
        item.quantity        = 0
      end
    end
  end

  def initiate_attendees
    return if @order.attendees.present?

    @order.order_items.each do |item|
      item['quantity'].times do
        @order.attendees.build do |attendee|
          attendee.event              = @event
          attendee.ticket_category_id = item['ticket_category_id']
          initiate_custom_infos(attendee)
        end
      end
    end
  end

  def initiate_custom_infos(attendee)
    @event.form_custom_fields.each do |field|
      attendee.attendee_custom_infos.build(
        form_custom_field: field,
        field_name: field.name
      )
    end
  end
end
