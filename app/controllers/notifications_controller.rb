class NotificationsController < ApplicationController
  def index
    if current_user.nil?
      render(json: { status: :not_found })
      return
    end

    @notifications = Notification.where(read_at: nil, recipient_id: current_user.id).includes(:notifiable)

    render(json: @notifications)
  end

  def mark_as_read
    notif_id = params[:id]
    notification = Notification.find_by(id: notif_id)

    if notification
      notification.update(read_at: Time.zone.now)
      render(json: { status: :ok })
    else
      render(json: { status: :not_found })
    end
  end
end
