class Events::TicketCategoriesController < Events::BaseController
  def index
    @ticket_categories = @event.ticket_categories.order(created_at: :asc)
    @ticket_category = TicketCategory.new
  end

  def new
    @event_slug = @event.slug
    @ticket_category = TicketCategory.new
  end

  def create
    @ticket_category = TicketCategory.new(ticket_category_params)

    if @ticket_category.save
      redirect_to :back
    else
      render :index
    end
  end

  def edit
    @ticket_category = TicketCategory.find(params[:id])
    respond_to do |format|
      format.js
    end
  end

  def destroy
    @ticket_category = TicketCategory.find(params[:id])

    if @ticket_category.event.attendees.where(ticket_category_id: @ticket_category.id).present?
      flash[:alert] = 'Cannot Delete ticket category with registered participants'
    else
      @ticket_category.destroy
    end

    redirect_to edit_event_path(slug: @event.slug)
  end

  def update
    @ticket_category = TicketCategory.find(params[:id])

    if @ticket_category.update(ticket_category_params)
      redirect_to :back
    else
      render :edit
    end
  end

  def ticket_category_params
    params.require(:ticket_category).permit(:category_name, :price, :quantity, :event_id)
  end
end
