class Events::TasksController < Events::BaseController
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  def index
    @task = Task.new(event_id: @event.id)
    @tasks = @event.tasks.includes(:users)
    @committees = committees_list
  end

  def new
    @task = Task.new
    @event = Event.friendly.find(params[:event_slug])
    @committees = committees_list
  end

  def my_tasks
    @pending_assignments = current_user.assignments.pending.includes(task: :users)
    @unfinished_tasks = current_user.tasks.unfinished
                                    .includes(:event)
                                    .includes(:users)
                                    .group_by { |t| t.event.name }

    @finished_tasks = current_user.tasks.finished
                                  .includes(:users)

  end

  def show
  end

  def create
    @task = Task.new(task_params)
    task_params[:user_ids].each do |user_id|
      Notification.create(recipient_id: user_id, actor: current_user, action: 'assigned', notifiable: @task) unless user_id.empty?
    end

    if @task.save
      redirect_to event_tasks_path(event_slug: @event.slug)
    else
      @tasks = @event.tasks
      @committees = committees_list
      flash[:danger] = @task.errors
      render 'index'
    end
  end

  def edit
    @committees = committees_list
  end

  def update
    old_recipient = @task.user_ids
    new_recipient = task_params[:user_ids] - old_recipient.map(&:to_s)
    new_recipient.each do |user_id|
      Notification.create(recipient_id: user_id, actor: current_user, action: 'assigned', notifiable: @task) unless user_id.empty?
    end

    if @task.update(task_params)
      redirect_to event_tasks_path
    else
      render :edit
    end
  end

  def destroy
    @task.destroy

    redirect_to event_tasks_path(event_slug: @event.slug)
  end

  def set_done
    @task = Task.find(params[:task_id])
    @task.done = true

    redirect_to event_tasks_path(event_slug: @event.slug)
  end

  private

  def set_task
    @task = Task.find(params[:id])
  end

  def task_params
    params.require(:task).permit(:name, :due_date, :task_list_id, :event_id, :done, user_ids: [])
  end

  def committees_list
    @event.committees.includes(:user).map do |c|
      committee = "#{c.user.fullname} <#{c.user.email}>"
      [committee, c.user.id]
    end
  end
end
