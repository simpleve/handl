class Events::BaseController < ApplicationController
  before_action :authenticate_user!
  before_action :set_event
  layout 'sub_sidebar'

  private

  def set_event
    @event = current_user.events.friendly.find(params[:event_slug])
    raise ActiveRecord::RecordNotFound unless @event
  end
end
