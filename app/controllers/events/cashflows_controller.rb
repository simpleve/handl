class Events::CashflowsController < Events::BaseController
  def index
    @cashflows = @event.cashflows.includes(:order)

    @income = @cashflows.where(account: 'income')
    @expense = @cashflows.where(account: 'expense')
    @total_income = @income.sum(:amount)
    @total_expense = @expense.sum(:amount)
    @balance = @total_income - @total_expense

    @reimbursed = @event.cashflows.includes(:order).where(account: 'reimburse')
    @total_reimbursed = @reimbursed.sum(:amount)

    @cashflows = @cashflows.filter_by_account(params[:filter])
                           .sort_by_attribute(params[:sort], params[:dir])
                           .page(params[:page])
  end

  def submit_reimbursement_request
    @cashflows = @event.cashflows
    @total_income = @cashflows.where(account: 'income').sum(:amount)
    @total_expense = @cashflows.where(account: 'expense').sum(:amount)
    @balance = @total_income - @total_expense

    if params[:reimbursement][:amount].to_i <= @balance
      committees = @event.committees.map { |c| c.user.email }
      AdminMailer.reimbursement_notice(current_user, params[:reimbursement]).deliver_later
      UserMailer.reimbursement_notice(current_user, committees, params[:reimbursement]).deliver_later
      flash[:success] = 'Your reimbursement request submitted. Please wait while our team review your request.'
    else
      flash[:alert] = 'You don\'t have that much balance, sorry.'
    end
    redirect_to event_cashflows_path(event_slug: params[:reimbursement][:event_slug])
  end
end
