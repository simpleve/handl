class Events::AttendeesController < Events::BaseController
  skip_before_action :authenticate_user!, only: [:attendance_confirmation]
  skip_before_action :set_event, only: [:attendance_confirmation]
  layout proc { ['attendance_confirmation'].include?(action_name) ? 'landing_page' : 'sub_sidebar' }

  def index
    @attendees = @event.attendees.includes(:ticket_category, :order)
                       .order(created_at: :asc)
    @attendee = Attendee.new(event: @event)
    respond_to do |format|
      format.html
      format.xls
    end
  end

  def new
    @attendees = Attendee.where(event_id: @event.id).includes(:ticket_category)
    @attendee = Attendee.new
    @form_custom_fields = @event.form_custom_fields
  end

  def create
    @attendee = Attendee.new(attendee_params)

    if RegisterAttendeeService.new(@attendee, @attendee.ticket_category, 'offline').call
      redirect_to event_attendees_path(event_slug: params[:event_slug])
    else
      @attendees = Attendee.where(event_id: @event.id)
      redirect_to :back
    end
  end

  def show
    @attendee = Attendee.find(params[:id])
    @form_custom_fields = @attendee.event.form_custom_fields

  end

  def edit
    @attendee = Attendee.find(params[:id])
  end

  def update
    @attendee = Attendee.find(params[:id])

    if @attendee.update(attendee_params)
      redirect_to event_attendees_path(event_slug: params[:event_slug])
    else
      render :edit
    end
  end

  def destroy
    @attendee = Attendee.find(params[:id])
    event = @attendee.event
    @attendee.destroy
    redirect_to event_attendees_path(event_slug: event.slug)
  end

  def attendee_delete
    @attendee = Attendee.find(params[:id])
    @attendee.destroy
  end

  def attendance_confirmation
    token = params[:token]
    confirmation = params[:confirmation]
    @attendee = Attendee.includes(:event).find_by(token: token)

    if @attendee.nil?
      render 'not_found'
      return
    end

    unless @attendee.status == 'confirmation_sent'
      render 'already_confirmed'
      return
    end

    if confirmation == 'cancelled'
      unconfirmed_attendees = Attendee.where(event_id: @attendee.event_id, status: :unconfirmed)
      if unconfirmed_attendees.present?
        next_unconfirmed_attendee = unconfirmed_attendees.shift
        next_unconfirmed_attendee.send_attendance_confirmation_email
      end
    elsif confirmation == 'confirmed'
      begin
        TicketIssuer.new(@attendee, @attendee.ticket_category).issue_ticket
      rescue TicketNotAvailable
        render 'quota_full' and return
      end
    end

    @attendee.update(status: confirmation)
  end

  def send_manual_confirmation
    attendee = Attendee.find(params[:attendee_id])

    attendee.send_attendance_confirmation_email if attendee.unconfirmed?

    render json: { status: 'confirmation_sent', attendee_id: attendee.id }
  end

  def attend
    @attendee = Attendee.find_by(id: params[:attendee_id])

    @attendee.attended!

    redirect_to event_attendees_path(event_slug: params[:event_slug])
  end

  private

  def attendee_params
    params.require(:attendee).permit(:name, :email, :company, :phone, :event_id, :ticket_category_id, :token, :confirmation, attendee_custom_infos_attributes: [:id, :field_name, :value, :form_custom_field_id,  :attachment])
  end
end
