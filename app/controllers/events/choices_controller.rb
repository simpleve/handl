class Events::ChoicesController < Events::BaseController
  def create
    @choice = Choice.new(choice_params)
    if @choice.save
      @form_custom_field = @choice.form_custom_field
      choiceIndex = @choice.form_custom_field.choices.sort { |a,b| a.created_at <=> b.created_at }.index(@choice)
      respond_to do |format|
        format.js
      end
    else
      redirect_to :back
    end
  end

  def destroy
    @choice = Choice.find(params[:id])
    @form_custom_field = @choice.form_custom_field
    #event_id = @choice.event_id
    @choice.destroy
    respond_to do |format|
      format.js
    end
  end

  def choice_params
    params.require(:choice).permit(:choice_name, :form_custom_field_id)
  end
end
