class Events::FormCustomFieldsController < Events::BaseController
  def index
    if @event.registration_closed_at
      if (DateTime.now > @event.registration_closed_at) && @event.force_open == false
        @event.registration_open = false
        @event.save
      end
    end
    @link = event_orders_url
    @form_custom_fields = @event.form_custom_fields
    @form_custom_field = FormCustomField.new
    respond_to do |format|
      format.html
      format.json
    end
  end

  def new
    @form_custom_field = FormCustomField.new(event_id: @event.id)
    if @form_custom_field.save
      #redirect_to event_form_custom_fields_path(@event)
      render "index"
    end
  end

  def edit
    @form_custom_field = FormCustomField.find(params[:id])
  end

  def create
    @form_custom_field = FormCustomField.new(form_custom_field_params)

    if @form_custom_field.save
      respond_to do |format|
        format.js
      end
    else
      render :index
    end
  end

  def update
    @form_custom_field = FormCustomField.find(params[:id])
    if params[:show_modal]
      @show_modal = params[:show_modal]
    else
      @show_modal = true
    end

    if @form_custom_field.update(form_custom_field_params)
      respond_to do |format|
        format.js
      end
    else
      render :edit
    end
  end

  def destroy
    @form_custom_field = FormCustomField.find(params[:id])
    event = @form_custom_field.event
    @form_custom_field.destroy
    respond_to do |format|
      format.js
    end
  end

  def form_custom_field_params
    params.require(:form_custom_field).permit(:name, :description, :event_id, :field_type, :required, choices_attributes: [:id, :choice_name])
  end

end
