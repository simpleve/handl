class Events::CommitteesController < Events::BaseController
  before_action :set_committee, only: [:show, :destroy]

  def index
    @committees = @event.committees.includes(:user)
    @committee = Committee.new(event_id: @event.id)
  end

  def show
    @user = Committee.find(params[:id]).user

    @pending_assignments = @user.assignments.includes(:task)
                                .where(tasks: { event_id: @event.id })
                                .pending
    @unfinished_tasks = @user.tasks.unfinished
                             .where(event_id: @event.id)
                             .includes(:event)
                             .includes(:users)
                             .group_by { |t| t.event.name }

    @finished_tasks = @user.tasks.finished
                           .where(event_id: @event.id)
                           .includes(:users)
  end

  def new
  end

  def create
    @user = User.find_or_create_by(email: committee_params[:email])

    if @user.valid? && @event.committees.create(user: @user)
      redirect_to event_committees_path(event_slug: params[:event_slug])
    else
      render :new
    end
  end

  def destroy
    event = @committee.event
    @committee.destroy
    respond_to do |format|
      if @committee.user == current_user
        format.html
        redirect_to events_path
      else
        format.js { render layout: false, content_type: 'text/javascript' }
      end
    end
  end

  private

  def set_committee
    @committee = Committee.find(params[:id])
  end

  def committee_params
    params.require(:committee).permit(:event_id, :user_id, :email)
  end
end
