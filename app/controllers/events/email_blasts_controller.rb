class Events::EmailBlastsController < Events::BaseController

  def index
    @email_blast = EmailBlast.new
    @recipient = []
    @recipient << @event.attendees.unconfirmed
    @recipient << @event.attendees.confirmation_sent
    @recipient << @event.attendees.confirmed
    @recipient << @event.attendees.cancelled
  end

  def create
    @email_blast = EmailBlast.new(email_blast_params)

    if @email_blast.save
      @attendees = Event.find(@email_blast.event_id).attendees

      @attendees.each do |attendee|
        @email_blast.send_email(attendee).deliver_later
      end
      render :index
    else
      #TODO : add handler if saving failed
      render :root
    end
  end

  def email_blast_params
    params.require(:email_blast).permit(:recipient, :subject, :content, :event_id)
  end
end
