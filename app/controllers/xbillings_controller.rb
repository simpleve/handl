class XbillingsController < ApplicationController
  def create
    @order = Order.find_by(id: params[:id])
    billing_attendee = @order.attendees.where(id: params[:billing_attendee_id]).first
    @order.billing_name = billing_attendee.name
    @order.billing_email = billing_attendee.email
    @order.save

    redirect_to order_check_out_event_order_path
  end
end
