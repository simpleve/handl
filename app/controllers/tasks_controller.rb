class TasksController < ApplicationController
  before_action :set_task, only: [:show, :edit, :update, :destroy]

  def my_tasks
    @pending_assignments = current_user.assignments.pending.includes(task: :users)
    @unfinished_tasks = current_user.tasks.unfinished
                                    .includes(:event)
                                    .includes(:users)
                                    .group_by { |t| t.event.name }

    @finished_tasks = current_user.tasks.finished
                                  .includes(:users)
  end
end
