class EventPresenter
  def initialize(event, user = nil)
    @event = event
    @user = user
  end

  def logo
    @event.logo? ? @event.logo_url : 'logogram-light.png'
  end

  def poster
    @event.poster? ? @event.poster_url : 'poster-example.jpg'
  end

  def location
    @event.location? ? @event.location : 'to be confirmed'
  end

  def organized_by
    @event.organized_by? ? @event.organized_by : 'unknown'
  end

  def description
    @event.description? ? @event.description : "There isn't any description yet."
  end

  def contact_name
    @event.contact_name? ? @event.contact_name : "our committee"
  end

  def user_can_edit?
    EventCommittee.new(@event, @user).is_committee?
  end

  def contact_info_humanize
    contacts = []

    contacts << "SMS or Call to #{contact_name} at #{@event.contact_phone}" if @event.contact_phone.present?
    contacts << "Email to #{@event.contact_email}" if @event.contact_email.present?

    contacts.join(' or via ')
  end
end
