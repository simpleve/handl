# == Schema Information
#
# Table name: notifications
#
#  id              :integer          not null, primary key
#  notifiable_id   :integer
#  notifiable_type :string
#  read_at         :datetime
#  actor_id        :integer
#  recipient_id    :integer
#  action          :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_notifications_on_actor_id                           (actor_id)
#  index_notifications_on_notifiable_type_and_notifiable_id  (notifiable_type,notifiable_id)
#  index_notifications_on_recipient_id                       (recipient_id)
#

class NotificationSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :message, :notifiable, :notifiable_url, :notifiable_type

  def notifiable_url
    '#'
  end
end
