class API::V1::EventSerializer < API::V1::BaseSerializer
  attributes :id, :name, :start_at, :end_at, :location, :organized_by, :slug, :logo
end
