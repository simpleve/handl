class API::V1::TaskSerializer < API::V1::BaseSerializer
  attributes :id, :name, :url

  def url
    api_v1_task_url(object)
  end
end
