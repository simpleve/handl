class API::V1::TicketCategorySerializer < API::V1::BaseSerializer
  attributes :id, :category_name, :price, :quantity, :event_id, :created_at, :updated_at, :attended_count, :confirmed_count

  def attended_count
    object.attendees.where('status = ?', Attendee.statuses[:attended]).count
  end

  def confirmed_count
    object.attendees.where('status = ? OR status = ?', Attendee.statuses[:confirmed], Attendee.statuses[:attended]).count
  end
end
