class API::V1::TaskListSerializer < API::V1::BaseSerializer
  attributes :id, :name, :url

  has_many :tasks

  def url
    api_v1_task_list_url(object)
  end
end
