Rails.application.routes.draw do
  devise_for :users, controllers: {
    omniauth_callbacks: 'omniauth_callbacks',
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords',
    confirmations: 'users/confirmations'
  }

  devise_scope :user do
    match '/users/confirmation', to: 'users/confirmations#update', via: :put
  end

  authenticated :user do
    root to: redirect('/events'), as: :authenticated_user
  end

  get 'orders/event/:event_slug', to: 'orders#new'
  get '/events/:event_slug/orders', to: 'orders#new', as: :event_ticket_order
  get '/events/new_ticket', to: 'events#new_ticket'
  get '/events/save_ticket', to: 'events#save_ticket'
  resources :events, param: :slug do
    put 'update_slug', to: 'events#update_slug'
    get 'open_close_registration'
    get 'overview'
    resources :orders do
      member do
        get 'attendees/new', to: 'orders#attendee_form', as: :order_attendee_form
        post 'attendees', to: 'orders#submit_attendee'
        get 'attendees_confirmation', to: 'orders#attendees_confirmation'
        get 'payment_methods', to: 'orders#payment_methods'
        post 'payment_methods', to: 'invoices#create'
        get 'confirmation', to: 'orders#confirmation', as: :order_confirmation
        post 'confirmation', to: 'orders#confirm'
        get 'check_out', to: 'orders#check_out', as: :order_check_out
      end
    end
    resources :email_blasts, module: 'events'
    resources :form_custom_fields, module: 'events'
    resources :committees, module: 'events'
    resources :choices, module: 'events'
    resources :attendees, module: 'events' do
      post 'send_manual_confirmation'
      delete 'attendee_delete'
      get 'attend'
    end
    resources :ticket_categories, module: 'events'
    resources :tasks, module: 'events' do
      get 'set_done'
    end
    resources :cashflows, module: 'events'
  end
  resources :assignments do
    match '/accept', to: 'assignments#accept', via: :get
  end
  resources :users
  resources :notifications do
    member do
      match '/mark_as_read', to: 'notifications#mark_as_read', via: :get
    end
  end
  root 'static_pages#home'
  get '/privacy_policy', to: 'static_pages#privacy_policy'
  get '/terms_and_conditions', to: 'static_pages#terms_and_conditions'
  get '/pricing', to: 'static_pages#pricing'
  get '/features', to: 'static_pages#features'
  get '/about_us', to: 'static_pages#about_us'
  get '/faq', to: 'static_pages#faq'

  namespace :api, path: '/', constraints: { subdomain: 'api' }, defaults: { format: :json } do
    namespace :v1 do
      resources :events, param: :slug do
        resources :attendees do
          collection do
            get '/verify_ticket/:token', to: 'attendees#verify_ticket'
          end
        end
        resources :ticket_categories
      end
      resources :task_lists
      resources :tasks
      resources :authentication, only: [:create]
    end
  end

  get '/my_tasks', to: 'tasks#my_tasks', module: 'events'
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  match "/delayed_job" => DelayedJobWeb, :anchor => false, via: [:get, :post]

  get '/attendance_confirmation', to: 'events/attendees#attendance_confirmation'

  get '/tickets/preview', to: 'tickets#preview'
  post '/tickets/send', to: 'tickets#send_ticket'

  post '/payments/verify'
  post '/payments/notify'
  post '/payments/redirect'
  post '/payments/cancel'

  post '/reimbursement', to: 'events/cashflows#submit_reimbursement_request'

  get '/account-setting', to: 'accounts#edit', as: 'account_setting'
  patch '/account-setting', to: 'accounts#update'
end
