env :PATH, ENV['PATH']
set :output, 'log/cron.log'

every :day, at: '10:00pm' do
  # rake 'reminder:remind_ongoing_task'
  rake 'attendees:confirm_attendance'
  # rake 'attendees:remind'
end

every :day, at: '12:00am' do
  command 'backup perform -t backup_handl'
end
