source 'https://rubygems.org'

gem 'rails', '4.2.6'
gem 'pg'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

gem 'jquery-rails'
gem 'jquery-turbolinks'
gem 'turbolinks', '~> 5.0.0.beta'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc

group :development, :test do
  gem 'byebug'
  gem 'rspec-rails'
  gem 'shoulda-matchers'
  gem 'capybara'
  gem 'spring'
  gem 'guard-spring'
  gem 'guard-rspec', require: false
  gem 'factory_girl_rails'
  gem 'mina'
  gem 'faker'
end

group :development do
  gem 'web-console', '~> 2.0'

  gem 'bullet'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'guard-livereload'
  gem 'quiet_assets'

  # rails panel chrome extension
  gem 'meta_request'
  gem 'annotate'
end

gem 'foundation-rails'
gem 'font-awesome-sass', '~> 4.7.0'
gem 'chosen-rails'

gem 'devise'
gem 'omniauth-google-oauth2'
gem 'rails_admin'
gem 'active_model_serializers'
gem 'mailgun_rails'
gem 'delayed_job_active_record'
gem 'delayed_job_web'
gem 'daemons'

gem 'premailer-rails'
gem 'nokogiri'

gem 'skylight'

gem 'carrierwave'
gem 'paperclip'
gem 'mini_magick'
gem 'whenever', require: false
gem 'wicked_pdf'
gem 'wkhtmltopdf-binary'
gem 'rqrcode'
gem 'sentry-raven'
gem 'activerecord-session_store'
gem 'wicked'
gem 'friendly_id'
gem 'kaminari'
gem 'fog-aws'
gem 'figaro'
gem 'httparty'
gem 'aasm'
