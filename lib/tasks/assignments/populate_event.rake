namespace :assignments do
  desc 'Populate empty event in assignments table'
  task populate_event: :environment do
    @assignments = Assignment.all
    @assignments.each do |assignment|
      assignment.event_id = assignment.task.task_list.event_id
      assignment.save
    end
  end
end
