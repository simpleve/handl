namespace :reminder do
  desc 'remind ongoing task via email'
  task remind_ongoing_task: :environment do
    Assignment.joins(:task).where('tasks.done_at is null').pluck(:user_id).uniq.each do |user_id|
      WeeklyTaskReminderJob.perform_later(user_id)
    end
  end
end
