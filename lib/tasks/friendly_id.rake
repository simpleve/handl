namespace :friendly_id do
  desc "Generate friendly_id for existing events"
  task generate_friendly_id: :environment do
    Event.find_each do |event|
      puts "Generating friendly_id for #{event.name}"
      event.save
    end
  end
end
