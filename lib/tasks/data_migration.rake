namespace :data_migration do
  desc 'Migrate notification action into message'
  task notification_message: :environment do
    ActiveRecord::Base.transaction do
      Notification.all.each do |notif|
        if notif.actor.present?
          actor_name = notif.actor.fullname
          notif.message = "#{actor_name} assigned you a task."
          notif.save
        else
          notif.destroy
        end
      end
    end
  end

  desc 'Edit cashflows detail column'
  task edit_cashflows_detail: :environment do
    Cashflow.where(account: 'expense', sub_account: 'service_fee').destroy_all

    ActiveRecord::Base.transaction do
      Cashflow.joins(:order)
              .where('orders.status = ?', 'payment_success')
              .where(account: 'income')
              .each do |income|
        attendee_count = income.order.attendees.count
        cashflow = Cashflow.new(
          amount: 5000 * attendee_count,
          account: 'expense',
          sub_account: 'service_fee',
          event_id: income.event_id,
          order_id: income.order_id,
          created_at: income.created_at + 1.second,
          updated_at: income.updated_at + 1.second
        )

        cashflow.save
        print '.'
      end
      puts "\nService fee cashflow created"
    end
  end
end
