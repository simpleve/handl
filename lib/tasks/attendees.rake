namespace :attendees do
  desc 'send attendance confirmation email'
  task confirm_attendance: :environment do
    # use 1 day due to database using utc to save datetime
    events_id = Event.where('DATE(start_at) = ?', 1.days.from_now).pluck(:id)

    events_id.each do |event_id|
      ConfirmAttendanceJob.perform_now(event_id)
    end
  end

  desc 'generate token for already existing attendeees'
  task generate_token: :environment do
    Attendee.where(token: nil).each do |attendee|
      attendee.generate_token
      attendee.save
    end
  end

  desc 'remind attendees before event'
  task remind: :environment do
    events_id = Event.where('DATE(start_at) = ?', 1.days.from_now).pluck(:id)

    attendees_id = Attendee.where(event_id: events_id).pluck(:id)

    attendees_id.each do |attendee_id|
      EventReminderJob.perform_later(attendee_id)
    end
  end
end
