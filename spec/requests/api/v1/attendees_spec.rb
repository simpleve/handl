require 'rails_helper'

RSpec.describe 'Attendees API' do
  xit 'verifies ticket' do
    event = FactoryGirl.create(:event)
    ticket = FactoryGirl.create(:ticket_category, event: event)
    attendee = FactoryGirl.create(:attendee, ticket_category: ticket, event: event)
    attendee.generate_token
    attendee.save
    user = FactoryGirl.create(:user)
    user.generate_authentication_token!
    user.save

    headers = {
      'Authorization' => "Token token=#{user.auth_token}"
    }

    puts user.auth_token

    get "http://api.lvh.me:3000/v1/events/#{event.slug}/attendees/verify_ticket/#{attendee.token}", headers: headers

    expect(response.status).to eq(200)
  end
end
