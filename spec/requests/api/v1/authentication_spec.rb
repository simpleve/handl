require 'rails_helper'

RSpec.describe 'Authentication API' do
  it 'returns success with correct email and password' do
    FactoryGirl.create(:user, email: 'test@email.com', password: '123456')

    post 'http://api.lvh.me:3000/v1/authentication', { email: 'test@email.com', password: '123456' }

    expect(response).to be_success
  end

  it 'returns unprocessable entity with incorrect email and password' do
    FactoryGirl.create(:user, email: 'test@email.com', password: '123456')

    post 'http://api.lvh.me:3000/v1/authentication', { email: 'test@email.com', password: '654321' }

    expect(response.status).to eq(422)
  end

  it 'returns auth token with successful authentication' do
    FactoryGirl.create(:user, email: 'test@email.com', password: '123456')

    post 'http://api.lvh.me:3000/v1/authentication', { email: 'test@email.com', password: '123456' }

    json = JSON.parse(response.body)

    expect(json['payload']['auth_token']).to be_present
  end
end
