# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class TaskMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/confirmation_instructions
  def reminder_for
    TaskMailer.reminder_for(User.first)
  end
end
