# Preview all emails at http://localhost:3000/rails/mailers/attendee_mailer
class AttendeeMailerPreview < ActionMailer::Preview
  def registration_success
    AttendeeMailer.registration_success(Order.where(order_type: 'paid').last.attendees.first)
  end

  def confirm_attendance
    AttendeeMailer.confirm_attendance(Attendee.last.id)
  end

  def send_ticket
    AttendeeMailer.send_ticket(Attendee.last.id)
  end
end
