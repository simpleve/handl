# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/confirmation_instructions
  def confirmation_instructions
    UserMailer.confirmation_instructions(User.first, {})
  end

  def reset_password_instructions
    UserMailer.reset_password_instructions(User.first, 'abcdef', {})
  end

  def reimbursement_notice
    UserMailer.reimbursement_notice(User.last, [User.last.email], 'amount' => 1000000, 'event_slug' => 'line-follower')
  end
end
