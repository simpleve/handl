require 'rails_helper'

RSpec.describe MobileAuthenticatorService do
  it 'returns error when initialized with empty credential' do
    response = MobileAuthenticatorService.new({}).call
    expected_response = { status: 200, payload: nil, error: 'credentials are empty or incomplete'}

    expect(response).to eq(expected_response)
  end

  it 'returns a user JSON when email and password passed are valid' do
    user = FactoryGirl.create(:user, password: '123456')

    response = MobileAuthenticatorService.new(email: user.email, password: user.password).call

    user.reload

    expected_user_response = {
      "id": user.id,
      "fullname": user.fullname,
      "created_at": user.created_at,
      "updated_at": user.updated_at,
      "email": user.email,
      "provider": user.provider,
      "uid": user.uid,
      "profile_picture": nil,
      "auth_token": user.auth_token,
      "id_card": {
          "url": nil
      },
      "bank": nil,
      "bank_acc_number": nil
    }

    expect(response[:status]).to eq(200)
    expect(response[:payload]).to eq(expected_user_response.to_json)
    expect(response[:error]).to eq(nil)
  end
end
