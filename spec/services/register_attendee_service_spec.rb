require 'rails_helper'

RSpec.describe RegisterAttendeeService do
  it 'raises error when ticket quota is not available'
  it 'raises error when attendee data not valid'
end
