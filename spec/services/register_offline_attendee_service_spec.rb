require 'rails_helper'

RSpec.describe RegisterOfflineAttendeeService do
  before do
    @event = FactoryGirl.create(:event)
    ticket = FactoryGirl.create(:ticket_category, price: 20000)
    attendee = FactoryGirl.build(:attendee, event: @event, ticket_category: ticket)

    RegisterOfflineAttendeeService.new(attendee).call
  end

  it 'saves attendee with order' do
    expect(Order.count).to eq(1)
    expect(OrderItem.count).to eq(1)
    expect(Attendee.count).to eq(1)
  end

  it 'logs income cashflow' do
    income = @event.cashflows.includes(:order).where(account: 'income')
    expect(income.sum(:amount)).to eq(20000)
  end
end
