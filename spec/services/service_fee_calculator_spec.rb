require 'rails_helper'

RSpec. describe ServiceFeeCalculator do
  it 'calculates service fee' do
    number_of_tickets = 2
    ticket_price = 20_000
    service_fee = ServiceFeeCalculator.call(ticket_price, number_of_tickets)

    expect(service_fee).to eq(10_000)
  end

  it 'calculates service fee based on percentage if ticket price > 100_000' do
    number_of_tickets = 2
    ticket_price = 2_000_000
    service_fee = ServiceFeeCalculator.call(ticket_price, number_of_tickets)

    expect(service_fee).to eq(200_000)
  end

  it 'returns 0 if price is less than 10_000' do
    number_of_tickets = 2
    ticket_price = 7000
    service_fee = ServiceFeeCalculator.call(ticket_price, number_of_tickets)

    expect(service_fee).to eq(0)
  end

  it 'returns 0 if number of tickets is 0' do
    number_of_tickets = 0
    ticket_price = 70_000
    service_fee = ServiceFeeCalculator.call(ticket_price, number_of_tickets)

    expect(service_fee).to eq(0)
  end

  it 'calculates basic service fee correctly' do
    number_of_tickets = 6
    ticket_price = 20_000
    service_fee = ServiceFeeCalculator.call(ticket_price, number_of_tickets)

    expect(service_fee).to eq(30_000)
  end
end
