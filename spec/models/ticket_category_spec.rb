# == Schema Information
#
# Table name: ticket_categories
#
#  id            :integer          not null, primary key
#  category_name :string
#  price         :integer
#  quantity      :integer
#  event_id      :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_ticket_categories_on_event_id  (event_id)
#
# Foreign Keys
#
#  fk_rails_57e1f626f2  (event_id => events.id)
#

require 'rails_helper'

RSpec.describe TicketCategory, type: :model do
  it 'raises an error if ticket quantity below 0' do
    ticket = FactoryGirl.build(:ticket_category, quantity: -1)
    ticket.save

    expect(ticket.errors).to be_present
  end
end
