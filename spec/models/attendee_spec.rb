# == Schema Information
#
# Table name: attendees
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  email              :string           not null
#  event_id           :integer          not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  ticket_category_id :integer          not null
#  status             :integer          default(0)
#
# Indexes
#
#  index_attendees_on_event_id  (event_id)
#
# Foreign Keys
#
#  fk_rails_4037bda9d5  (event_id => events.id)
#  fk_rails_e8a1210330  (ticket_category_id => ticket_categories.id)
#

require 'rails_helper'

RSpec.describe Attendee, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
