require 'rails_helper'

RSpec.describe TicketIssuer, type: :model do
  describe '#issue_ticket' do
    let(:event) { FactoryGirl.create(:event) }
    let(:ticket) { FactoryGirl.create(:ticket_category, event: event) }
    let(:attendee) { FactoryGirl.create(:attendee, event: event, ticket_category: ticket, status: 'unconfirmed') }
    let(:issuer) { TicketIssuer.new(attendee, ticket) }

    context 'ticket quantity is more than 0' do
      before do
        ticket.update(quantity: 100)
        issuer.issue_ticket
      end

      it 'decreases quantity' do
        expect(ticket.quantity).to eq(99)
      end
    end

    context 'ticket quantity is 0' do
      before do
        ticket.update(quantity: 0)
      end

      it 'raises error' do
        expect { issuer.issue_ticket }.to raise_error(TicketNotAvailable)
      end
    end
  end
end
