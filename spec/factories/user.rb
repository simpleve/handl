FactoryGirl.define do
  factory :user do
    fullname Faker::Name.name
    email Faker::Internet.email
  end
end
