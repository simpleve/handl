# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170419141448) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assignments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "task_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "accepted_at"
  end

  add_index "assignments", ["task_id"], name: "index_assignments_on_task_id", using: :btree
  add_index "assignments", ["user_id"], name: "index_assignments_on_user_id", using: :btree

  create_table "attendee_custom_infos", force: :cascade do |t|
    t.string   "field_name"
    t.text     "value"
    t.integer  "attendee_id"
    t.integer  "form_custom_field_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
  end

  create_table "attendee_list_relations", force: :cascade do |t|
    t.integer  "attendee_id"
    t.integer  "attendee_list_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "attendee_lists", force: :cascade do |t|
    t.string   "list_name"
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "attendees", force: :cascade do |t|
    t.string   "name",                           null: false
    t.string   "email",                          null: false
    t.integer  "event_id",                       null: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "ticket_category_id",             null: false
    t.integer  "status",             default: 0
    t.integer  "order_id"
    t.string   "token"
    t.string   "phone"
    t.string   "company"
  end

  add_index "attendees", ["event_id"], name: "index_attendees_on_event_id", using: :btree

  create_table "cashflows", force: :cascade do |t|
    t.decimal  "amount",      precision: 10, scale: 2
    t.integer  "event_id"
    t.integer  "order_id"
    t.string   "account"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "detail"
    t.string   "sub_account"
  end

  add_index "cashflows", ["event_id"], name: "index_cashflows_on_event_id", using: :btree
  add_index "cashflows", ["order_id"], name: "index_cashflows_on_order_id", using: :btree

  create_table "choices", force: :cascade do |t|
    t.string   "choice_name"
    t.integer  "form_custom_field_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "committees", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "committees", ["event_id"], name: "index_committees_on_event_id", using: :btree
  add_index "committees", ["user_id"], name: "index_committees_on_user_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "email_blasts", force: :cascade do |t|
    t.string   "recipient"
    t.string   "subject"
    t.text     "content"
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.datetime "start_at"
    t.string   "logo"
    t.string   "description"
    t.string   "location"
    t.string   "poster"
    t.string   "organized_by"
    t.datetime "end_at"
    t.string   "slug"
    t.boolean  "registration_open",      default: true
    t.string   "contact_name"
    t.string   "contact_email"
    t.string   "contact_phone"
    t.string   "name_desc"
    t.string   "company_desc"
    t.string   "email_desc"
    t.string   "phone_desc"
    t.datetime "registration_closed_at"
    t.boolean  "force_open",             default: false
  end

  add_index "events", ["slug"], name: "index_events_on_slug", unique: true, using: :btree

  create_table "form_custom_fields", force: :cascade do |t|
    t.string   "name"
    t.boolean  "required",    default: false
    t.integer  "event_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "field_type",  default: 0,     null: false
    t.string   "description"
  end

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.decimal  "amount",         precision: 10, scale: 2, null: false
    t.string   "status",                                  null: false
    t.integer  "payment_code"
    t.decimal  "total_amount",   precision: 10, scale: 2, null: false
    t.integer  "event_id",                                null: false
    t.integer  "order_id",                                null: false
    t.datetime "expired_at"
    t.string   "uid"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "payment_method",                          null: false
  end

  add_index "invoices", ["event_id"], name: "index_invoices_on_event_id", using: :btree
  add_index "invoices", ["order_id"], name: "index_invoices_on_order_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.datetime "read_at"
    t.integer  "actor_id"
    t.integer  "recipient_id"
    t.string   "message"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "notifications", ["actor_id"], name: "index_notifications_on_actor_id", using: :btree
  add_index "notifications", ["notifiable_type", "notifiable_id"], name: "index_notifications_on_notifiable_type_and_notifiable_id", using: :btree
  add_index "notifications", ["recipient_id"], name: "index_notifications_on_recipient_id", using: :btree

  create_table "order_items", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "ticket_category_id"
    t.integer  "quantity"
    t.float    "price"
    t.float    "total"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "order_items", ["order_id"], name: "index_order_items_on_order_id", using: :btree
  add_index "order_items", ["ticket_category_id"], name: "index_order_items_on_ticket_category_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "total"
    t.string   "payment_code"
    t.string   "status"
    t.string   "billing_name"
    t.string   "billing_email"
    t.datetime "expired_at"
    t.integer  "paid_total"
    t.string   "order_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "event_id"
  end

  create_table "rooms", force: :cascade do |t|
    t.string   "current"
    t.string   "integer"
    t.string   "voltage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "task_lists", force: :cascade do |t|
    t.string   "name",       null: false
    t.integer  "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "task_lists", ["event_id"], name: "index_task_lists_on_event_id", using: :btree

  create_table "tasks", force: :cascade do |t|
    t.string   "name",         null: false
    t.datetime "due_date"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "task_list_id"
    t.datetime "done_at"
    t.integer  "event_id"
  end

  add_index "tasks", ["task_list_id"], name: "index_tasks_on_task_list_id", using: :btree

  create_table "ticket_categories", force: :cascade do |t|
    t.string   "category_name"
    t.integer  "price"
    t.integer  "quantity"
    t.integer  "event_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "ticket_categories", ["event_id"], name: "index_ticket_categories_on_event_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "fullname"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.string   "provider"
    t.string   "uid"
    t.string   "profile_picture"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "auth_token"
    t.string   "id_card"
    t.string   "bank"
    t.string   "bank_acc_number"
  end

  add_index "users", ["auth_token"], name: "index_users_on_auth_token", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "assignments", "tasks"
  add_foreign_key "assignments", "users"
  add_foreign_key "attendees", "events"
  add_foreign_key "attendees", "orders"
  add_foreign_key "attendees", "ticket_categories"
  add_foreign_key "cashflows", "events"
  add_foreign_key "cashflows", "orders"
  add_foreign_key "committees", "events"
  add_foreign_key "committees", "users"
  add_foreign_key "invoices", "events"
  add_foreign_key "invoices", "orders"
  add_foreign_key "order_items", "orders"
  add_foreign_key "order_items", "ticket_categories"
  add_foreign_key "orders", "events"
  add_foreign_key "task_lists", "events"
  add_foreign_key "tasks", "events"
  add_foreign_key "ticket_categories", "events"
end
