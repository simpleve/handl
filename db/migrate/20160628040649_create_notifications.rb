class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :notifiable, polymorphic: true, index: true
      t.datetime :read_at
      t.integer :actor_id, index: true, foreign_key: true
      t.integer :recipient_id, index: true, foreign_key: true
      t.string :action

      t.timestamps null: false
    end
  end
end
