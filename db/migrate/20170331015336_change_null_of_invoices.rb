class ChangeNullOfInvoices < ActiveRecord::Migration
  def change
    change_column_null :invoices, :event_id, false
    change_column_null :invoices, :order_id, false
    change_column_null :invoices, :amount, false
    change_column_null :invoices, :total_amount, false
    change_column_null :invoices, :status, false
  end
end
