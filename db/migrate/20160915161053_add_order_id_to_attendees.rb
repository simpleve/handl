class AddOrderIdToAttendees < ActiveRecord::Migration
  def change
    add_column :attendees, :order_id, :integer
    add_foreign_key :attendees, :orders, column: 'order_id'
  end
end
