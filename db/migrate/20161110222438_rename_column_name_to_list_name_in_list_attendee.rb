class RenameColumnNameToListNameInListAttendee < ActiveRecord::Migration
  def change
    rename_column :attendee_lists, :name, :list_name
  end
end
