class AddTicketCategoryIdToAttendees < ActiveRecord::Migration
  def change
    add_column :attendees, :ticket_category_id, :integer
    add_foreign_key :attendees, :ticket_categories, column: 'ticket_category_id'

    change_column_null :attendees, :name, false
    change_column_null :attendees, :email, false
    change_column_null :attendees, :ticket_category_id, false
    change_column_null :attendees, :event_id, false
  end
end
