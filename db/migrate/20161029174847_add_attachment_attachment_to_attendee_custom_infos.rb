class AddAttachmentAttachmentToAttendeeCustomInfos < ActiveRecord::Migration
  def self.up
    change_table :attendee_custom_infos do |t|
      t.attachment :attachment
    end
  end

  def self.down
    remove_attachment :attendee_custom_infos, :attachment
  end
end
