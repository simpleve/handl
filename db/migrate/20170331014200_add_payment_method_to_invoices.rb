class AddPaymentMethodToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :payment_method, :integer, null: false
  end
end
