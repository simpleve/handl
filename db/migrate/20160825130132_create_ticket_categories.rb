class CreateTicketCategories < ActiveRecord::Migration
  def change
    create_table :ticket_categories do |t|
      t.string :category_name
      t.integer :price
      t.integer :quantity
      t.references :event, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
