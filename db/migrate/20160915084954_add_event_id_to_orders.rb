class AddEventIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :event_id, :integer
    add_foreign_key :orders, :events, column: 'event_id'
  end
end
