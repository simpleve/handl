class ChangeFieldTypeFromStringToInteger < ActiveRecord::Migration
  def change
    remove_column :form_custom_fields, :field_type, :string, null: false
    add_column :form_custom_fields, :field_type, :integer, default: 0, null: false
  end
end
