class CreateEmailBlasts < ActiveRecord::Migration
  def change
    create_table :email_blasts do |t|
      t.string :recipient
      t.string :subject
      t.text :content
      t.integer :event_id

      t.timestamps null: false
    end
  end
end
