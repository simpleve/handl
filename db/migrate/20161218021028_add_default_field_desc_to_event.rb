class AddDefaultFieldDescToEvent < ActiveRecord::Migration
  def change
    add_column :events, :name_desc, :string
    add_column :events, :company_desc, :string
    add_column :events, :email_desc, :string
    add_column :events, :phone_desc, :string
  end
end
