class ChangeActionToMessageInNotifications < ActiveRecord::Migration
  def change
    rename_column :notifications, :action, :message
  end
end
