class CreateTaskLists < ActiveRecord::Migration
  def change
    create_table :task_lists do |t|
      t.string :name, null: false
      t.references :event, index: true, foreign_key: true

      t.timestamps null: false
    end

    add_column :tasks, :task_list_id, :integer
    add_index :tasks, :task_list_id
  end
end
