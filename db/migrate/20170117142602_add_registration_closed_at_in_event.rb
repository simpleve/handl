class AddRegistrationClosedAtInEvent < ActiveRecord::Migration
  def change
    add_column :events, :registration_closed_at, :datetime
  end
end
