class AddColumnRegistrationOpenInEvents < ActiveRecord::Migration
  def change
    add_column :events, :registration_open, :boolean, default: false
  end
end
