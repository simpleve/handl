class CreateChoices < ActiveRecord::Migration
  def change
    create_table :choices do |t|
      t.string :name
      t.integer :form_custom_field_id

      t.timestamps null: false
    end
  end
end
