class AddColumnCompanyToAttendees < ActiveRecord::Migration
  def change
    add_column :attendees, :company, :string
  end
end
