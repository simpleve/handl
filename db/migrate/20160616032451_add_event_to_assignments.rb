class AddEventToAssignments < ActiveRecord::Migration
  def change
    add_reference :assignments, :event, index: true, foreign_key: true
  end
end
