class AddAcceptedAtToAssignments < ActiveRecord::Migration
  def change
    add_column :assignments, :accepted_at, :datetime
  end
end
