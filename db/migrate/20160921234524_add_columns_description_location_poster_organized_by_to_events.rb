class AddColumnsDescriptionLocationPosterOrganizedByToEvents < ActiveRecord::Migration
  def change
    add_column :events, :description, :string
    add_column :events, :location, :string
    add_column :events, :poster, :string
    add_column :events, :organized_by, :string
  end
end
