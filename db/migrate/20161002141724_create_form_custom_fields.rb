class CreateFormCustomFields < ActiveRecord::Migration
  def change
    create_table :form_custom_fields do |t|
      t.string :name
      t.boolean :required
      t.integer :event_id
      t.string :type

      t.timestamps null: false
    end
  end
end
