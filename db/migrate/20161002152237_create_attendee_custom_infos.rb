class CreateAttendeeCustomInfos < ActiveRecord::Migration
  def change
    create_table :attendee_custom_infos do |t|
      t.string :field_name
      t.text :value
      t.integer :attendee_id
      t.integer :form_custom_field_id

      t.timestamps null: false
    end
  end
end
