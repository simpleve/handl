class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :total
      t.string :payment_code
      t.string :status
      t.string :billing_name
      t.string :billing_email
      t.datetime :expired_at
      t.integer :paid_total
      t.string :order_type

      t.timestamps null: false
    end
  end
end
