class ChangeTypeColumnNameInFormCustomFields < ActiveRecord::Migration
  def change
    rename_column :form_custom_fields, :type, :field_type
  end
end
