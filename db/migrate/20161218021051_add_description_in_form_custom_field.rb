class AddDescriptionInFormCustomField < ActiveRecord::Migration
  def change
    add_column :form_custom_fields, :description, :string
  end
end
