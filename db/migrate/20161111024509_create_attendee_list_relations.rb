class CreateAttendeeListRelations < ActiveRecord::Migration
  def change
    create_table :attendee_list_relations do |t|
      t.integer :attendee_id
      t.integer :attendee_list_id

      t.timestamps null: false
    end
  end
end
