class AddTypeColumnInFormCustomFields < ActiveRecord::Migration
  def change
    add_column :form_custom_fields, :type, :integer
  end
end
