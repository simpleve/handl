class AddStatusToAttendees < ActiveRecord::Migration
  def change
    add_column :attendees, :status, :integer, default: 0
  end
end
