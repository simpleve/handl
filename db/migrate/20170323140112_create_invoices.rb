class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.decimal :amount, precision: 10, scale: 2
      t.string :status
      t.integer :payment_code
      t.decimal :total_amount, precision: 10, scale: 2
      t.references :event, index: true, foreign_key: true
      t.references :order, index: true, foreign_key: true
      t.datetime :expired_at
      t.string :uid

      t.timestamps null: false
    end
  end
end
