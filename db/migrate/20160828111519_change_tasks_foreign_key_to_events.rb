class ChangeTasksForeignKeyToEvents < ActiveRecord::Migration
  def change
    add_column :tasks, :event_id, :integer
    add_foreign_key :tasks, :events, column: 'event_id'

    Task.all.each do |task|
      if task.task_list.present? && task.task_list.event.present?
        task.update(event_id: task.task_list.event.id)
      end
    end
  end
end
