class FormCustomFieldsSetRequiredDefaultValueToFalse < ActiveRecord::Migration
  def change
    change_column_default :form_custom_fields, :required, false
  end
end
