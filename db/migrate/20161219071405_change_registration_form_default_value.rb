class ChangeRegistrationFormDefaultValue < ActiveRecord::Migration
  def change
    change_column :events, :registration_open, :boolean, default: true
  end
end
