class CreateAttendeeLists < ActiveRecord::Migration
  def change
    create_table :attendee_lists do |t|
      t.string :name
      t.integer :event_id

      t.timestamps null: false
    end
  end
end
