class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.references :order, index: true, foreign_key: true
      t.references :ticket_category, index: true, foreign_key: true
      t.integer :quantity
      t.float :price
      t.float :total

      t.timestamps null: false
    end
  end
end
