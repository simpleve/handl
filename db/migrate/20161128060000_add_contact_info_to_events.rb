class AddContactInfoToEvents < ActiveRecord::Migration
  def change
    add_column :events, :contact_name, :string
    add_column :events, :contact_email, :string
    add_column :events, :contact_phone, :string
  end
end
