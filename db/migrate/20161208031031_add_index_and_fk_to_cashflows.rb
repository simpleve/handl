class AddIndexAndFkToCashflows < ActiveRecord::Migration
  def change
    add_foreign_key :cashflows, :events, column: :event_id
    add_foreign_key :cashflows, :orders, column: :order_id

    add_index :cashflows, :event_id
    add_index :cashflows, :order_id
  end
end
