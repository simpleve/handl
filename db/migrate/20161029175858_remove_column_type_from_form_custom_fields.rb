class RemoveColumnTypeFromFormCustomFields < ActiveRecord::Migration
  def change
    remove_column :form_custom_fields, :type
  end
end
