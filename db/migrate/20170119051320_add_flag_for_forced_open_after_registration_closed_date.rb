class AddFlagForForcedOpenAfterRegistrationClosedDate < ActiveRecord::Migration
  def change
    add_column :events, :force_open, :boolean, default: false
  end
end
