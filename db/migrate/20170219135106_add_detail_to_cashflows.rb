class AddDetailToCashflows < ActiveRecord::Migration
  def change
    add_column :cashflows, :detail, :string
    add_column :cashflows, :sub_account, :string
  end
end
