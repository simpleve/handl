class ChangeColumnNameInChoice < ActiveRecord::Migration
  def change
    rename_column :choices, :name, :choice_name
  end
end
