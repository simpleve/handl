class AddDetailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :id_card, :string
    add_column :users, :bank, :string
    add_column :users, :bank_acc_number, :string
  end
end
