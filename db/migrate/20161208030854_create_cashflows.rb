class CreateCashflows < ActiveRecord::Migration
  def change
    create_table :cashflows do |t|
      t.decimal :amount, precision: 10, scale: 2
      t.integer :event_id
      t.integer :order_id
      t.string :account

      t.timestamps null: false
    end
  end
end
